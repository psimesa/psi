#!/bin/bash

dir="psi"
repository="https://obritomatheus@bitbucket.org/psimesa/psi.git"
app_name="psi"
app_port=8080
app_environment="homolog"
docker_port=8080
docker_image=$app_name:$app_environment


echo -e "###################################\n#\tDeploy on Homolog         #\n################################### \n\n"


echo -e "\e[92mSTEP 1. UPDATING REPOSITORY"
    if [ -d "~/Workspace/workspace-psi/$dir" ]
    then
        cd ~/Workspace/workspace-psi/"$dir"
	echo -e "selected dir:" $(pwd)
	echo "Updating artifacts"
        sudo git pull
    else
	mkdir -p ~/Workspace/workspace-psi
        cd ~/Workspace/workspace-psi/
        echo "Cloning repository"
        sudo git clone $repository
    fi


echo -e "\n\e[95mSTEP 2. GENERATING APP"
    cd ~/Workspace/workspace-psi/"$dir"
    sudo rm -rf target
    mvn clean install -U -DskipTests=true

echo -e "\n\e[91mSTEP 3. STOPING CONTAINER"
    if [[ -n "docker ps -qa --filter "name=$app_name"" ]];
    then
        docker stop  $app_name
    fi


echo -e "\n\e[90mSTEP 4. GENERATING IMAGE DOCKER"
    docker rmi -f "$(docker images --filter=reference="$app_name":"$app_environment" -q)"
    docker build . -t $docker_image


echo -e "\e[93mSTEP 5. RUNNIG CONTAINER"
    if [[ -n "docker ps -qa --filter "name=$app_name"" ]];
    then
        docker rm -f $app_name
        docker run -dti -p $app_port:$docker_port --name=$app_name $docker_image
    else
        docker run -dti -p $app_port:$docker_port --name=$app_name $docker_image
    fi

