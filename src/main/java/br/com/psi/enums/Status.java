package br.com.psi.enums;

public enum Status {

	NEW, BLOOMBERG, EXECUTING, FINISHED

}
