package br.com.psi.enums;

public enum TipoOrdem {

	ZERAR(1),
	EQUAL(2),
	COMPRA(3),
	VENDA(4),
	LS(5),
	ZERAR_V_C(6),
	NEUTRALIZAR(7);
	
	private Integer tipo;
	
	private TipoOrdem(Integer tipo) {
		this.tipo = tipo;
	}
	
	public Integer getTipo() {
		return tipo;
	}

}
