package br.com.psi.integration;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.MessageIterator;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;
import com.bloomberglp.blpapi.Subscription;
import com.bloomberglp.blpapi.SubscriptionList;
import com.google.gson.Gson;

import br.com.psi.dto.request.OrdemEmsxRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BloombergTerminalScheduled {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Value("${exchange.bloomberg}")
    private String exchange;
    
	@Value("${routeKey.bloomberg.receive-order}")
    private String receiveOrderBloomberg;

	private static final Name ORDER_ROUTE_FIELDS = new Name("OrderRouteFields");
	
    // SESSION_STATUS
    private static final Name SESSION_STARTED = new Name("SessionStarted");

	// SUBSCRIPTION_STATUS + SUBSCRIPTION_DATA
    private static final Name SUBSCRIPTION_STARTED = new Name("SubscriptionStarted");
    private static final Name SUBSCRIPTION_FAILURE = new Name("SubscriptionFailure");
    private static final Name SUBSCRIPTION_TERMINATED = new Name("SubscriptionTerminated");
    
    // SERVICE_STATUS
    private static final Name SERVICE_OPENED = new Name("ServiceOpened");
    private static final Name SERVICE_OPEN_FAILURE = new Name("ServiceOpenFailure");
	
    private Subscription orderSubscription;
    private Subscription routeSubscription;
    private CorrelationID orderSubscriptionID;
    private CorrelationID routeSubscriptionID;
    
    private String d_service;
    private String d_host;
    private int d_port;
    
    private static Session session = new Session();
    
    @Bean
    public void init() throws Exception{
    	BloombergTerminalScheduled example = new BloombergTerminalScheduled();
        example.run();
    }
    
	public BloombergTerminalScheduled()
    {
    	// Define the service required, in this case the EMSX beta service, 
    	// and the values to be used by the SessionOptions object
    	// to identify IP/port of the back-end process.
    	d_service = "//blp/emapisvc_beta";
    	d_host = "localhost";
        d_port = 8194;
    }
	
	private void run() throws Exception
    {
    	SessionOptions d_sessionOptions = new SessionOptions();
        d_sessionOptions.setServerHost(d_host);
        d_sessionOptions.setServerPort(d_port);
        session = new Session(d_sessionOptions);
        session.start();
    }
	
	@Scheduled(fixedDelay = 1000, initialDelay = 10000)
	private void processSessionEvent() throws Exception {
		MessageIterator msgIter = session.nextEvent().messageIterator();
        while (msgIter.hasNext()) {
            Message msg = msgIter.next();
            if(msg.messageType().equals(SESSION_STARTED)) {
                session.openServiceAsync(d_service);
            }
            if(msg.messageType().equals(SERVICE_OPENED)) {
                createOrderSubscription(session);
            } else if(msg.messageType().equals(SERVICE_OPEN_FAILURE)) {
                log.error("Error: Service failed to open");
            }
            if(msg.messageType().equals(SUBSCRIPTION_STARTED)) {
                if(msg.correlationID()==orderSubscriptionID) {
                    createRouteSubscription(session);
                } else if(msg.correlationID()==routeSubscriptionID) {
                    log.info("Route subscription started successfully");
                }
            } else if(msg.messageType().equals(SUBSCRIPTION_FAILURE)) {
                if(msg.correlationID()==orderSubscriptionID) {
                    log.error("Error: Order subscription failed");
                } else if(msg.correlationID()==routeSubscriptionID) {
                    log.error("Error: Route subscription failed");
                }
            } else if(msg.messageType().equals(SUBSCRIPTION_TERMINATED)) {
                if(msg.correlationID()==orderSubscriptionID) {
                    log.error("Order subscription terminated");
                } else if(msg.correlationID()==routeSubscriptionID) {
                    log.error("Route subscription terminated");
                }
            }
            if(msg.messageType().equals(ORDER_ROUTE_FIELDS)) {
                Integer event_status = msg.getElementAsInt32("EVENT_STATUS");
                if(event_status==1) {
                    //Heartbeat event, arrives every 1 second of inactivity on a subscription
                    if(msg.correlationID()==orderSubscriptionID) {
                        System.out.print("O."); 
                    } else if(msg.correlationID()==routeSubscriptionID) {
                        System.out.print("R."); 
                    }
                } else if(event_status==11) {
                    if(msg.correlationID()==orderSubscriptionID) {
                        log.error("Order - End of initial paint"); 
                    } else if(msg.correlationID()==routeSubscriptionID) {
                        log.error("Route - End of initial paint"); 
                    }
                } else {
                    if(msg.correlationID()==orderSubscriptionID && event_status != 4  && event_status != 8) {
                        //this.updateOrder(msg);
                    	Gson gson = new Gson();
                		rabbitTemplate.convertAndSend(exchange, receiveOrderBloomberg, gson.toJson(buildOrderEmsx(msg)));
                    } else if(msg.correlationID()==routeSubscriptionID) {
                        //Fila para enviar alteração da ordem
                         //msg @Paramr
                    }
                }
                
            } else {
                System.err.println("Error: Unexpected message");
            }
            
        }
	}
	
	private void createOrderSubscription(Session session) 
    {
        System.out.println("Create Order subscription");

        // Create the topic string for the order subscription. Here, we are subscribing 
        // to every available order field, however, you can subscribe to only the fields
        // required for your application.
        String orderTopic = d_service + "/order?fields=";
        
        orderTopic = orderTopic + "API_SEQ_NUM,";
        orderTopic = orderTopic + "EMSX_ACCOUNT,";
        orderTopic = orderTopic + "EMSX_AMOUNT,";
        orderTopic = orderTopic + "EMSX_ARRIVAL_PRICE,";
        orderTopic = orderTopic + "EMSX_ASSET_CLASS,";
        orderTopic = orderTopic + "EMSX_ASSIGNED_TRADER,";
        orderTopic = orderTopic + "EMSX_AVG_PRICE,";
        orderTopic = orderTopic + "EMSX_BASKET_NAME,";
        orderTopic = orderTopic + "EMSX_BASKET_NUM,";
        orderTopic = orderTopic + "EMSX_BLOCK_ID,";
        orderTopic = orderTopic + "EMSX_BROKER,";
        orderTopic = orderTopic + "EMSX_BROKER_COMM,";
        orderTopic = orderTopic + "EMSX_BSE_AVG_PRICE,";
        orderTopic = orderTopic + "EMSX_BSE_FILLED,";
        orderTopic = orderTopic + "EMSX_BUYSIDE_LEI,";
        orderTopic = orderTopic + "EMSX_CLIENT_IDENTIFICATION,";
        orderTopic = orderTopic + "EMSX_CFD_FLAG,";
        orderTopic = orderTopic + "EMSX_COMM_DIFF_FLAG,";
        orderTopic = orderTopic + "EMSX_COMM_RATE,";
        orderTopic = orderTopic + "EMSX_CURRENCY_PAIR,";
        orderTopic = orderTopic + "EMSX_DATE,";
        orderTopic = orderTopic + "EMSX_DAY_AVG_PRICE,";
        orderTopic = orderTopic + "EMSX_DAY_FILL,";
        orderTopic = orderTopic + "EMSX_DIR_BROKER_FLAG,";
        orderTopic = orderTopic + "EMSX_EXCHANGE,";
        orderTopic = orderTopic + "EMSX_EXCHANGE_DESTINATION,";
        orderTopic = orderTopic + "EMSX_EXEC_INSTRUCTION,";
        orderTopic = orderTopic + "EMSX_FILL_ID,";
        orderTopic = orderTopic + "EMSX_FILLED,";
        orderTopic = orderTopic + "EMSX_GPI,";
        orderTopic = orderTopic + "EMSX_GTD_DATE,";
        orderTopic = orderTopic + "EMSX_HAND_INSTRUCTION,";
        orderTopic = orderTopic + "EMSX_IDLE_AMOUNT,";
        orderTopic = orderTopic + "EMSX_INVESTOR_ID,";
        orderTopic = orderTopic + "EMSX_ISIN,";
        orderTopic = orderTopic + "EMSX_LIMIT_PRICE,";
        orderTopic = orderTopic + "EMSX_MIFID_II_INSTRUCTION,";
        orderTopic = orderTopic + "EMSX_MOD_PEND_STATUS,";
        orderTopic = orderTopic + "EMSX_NOTES,";
        orderTopic = orderTopic + "EMSX_NSE_AVG_PRICE,";
        orderTopic = orderTopic + "EMSX_NSE_FILLED,";
        orderTopic = orderTopic + "EMSX_ORD_REF_ID,";
        orderTopic = orderTopic + "EMSX_ORDER_AS_OF_DATE,";
        orderTopic = orderTopic + "EMSX_ORDER_AS_OF_TIME_MICROSEC,";
        orderTopic = orderTopic + "EMSX_ORDER_TYPE,";
        orderTopic = orderTopic + "EMSX_ORIGINATE_TRADER,";
        orderTopic = orderTopic + "EMSX_ORIGINATE_TRADER_FIRM,";
        orderTopic = orderTopic + "EMSX_PERCENT_REMAIN,";
        orderTopic = orderTopic + "EMSX_PM_UUID,";
        orderTopic = orderTopic + "EMSX_PORT_MGR,";
        orderTopic = orderTopic + "EMSX_PORT_NAME,";
        orderTopic = orderTopic + "EMSX_PORT_NUM,";
        orderTopic = orderTopic + "EMSX_POSITION,";
        orderTopic = orderTopic + "EMSX_PRINCIPAL,";
        orderTopic = orderTopic + "EMSX_PRODUCT,";
        orderTopic = orderTopic + "EMSX_QUEUED_DATE,";
        orderTopic = orderTopic + "EMSX_QUEUED_TIME,";
        orderTopic = orderTopic + "EMSX_QUEUED_TIME_MICROSEC,";
        orderTopic = orderTopic + "EMSX_REASON_CODE,";
        orderTopic = orderTopic + "EMSX_REASON_DESC,";
        orderTopic = orderTopic + "EMSX_REMAIN_BALANCE,";
        orderTopic = orderTopic + "EMSX_ROUTE_ID,";
        orderTopic = orderTopic + "EMSX_ROUTE_PRICE,";
        orderTopic = orderTopic + "EMSX_SEC_NAME,";
        orderTopic = orderTopic + "EMSX_SEDOL,";
        orderTopic = orderTopic + "EMSX_SEQUENCE,";
        orderTopic = orderTopic + "EMSX_SETTLE_AMOUNT,";
        orderTopic = orderTopic + "EMSX_SETTLE_DATE,";
        orderTopic = orderTopic + "EMSX_SI,";
        orderTopic = orderTopic + "EMSX_SIDE,";
        orderTopic = orderTopic + "EMSX_START_AMOUNT,";
        orderTopic = orderTopic + "EMSX_STATUS,";
        orderTopic = orderTopic + "EMSX_STEP_OUT_BROKER,";
        orderTopic = orderTopic + "EMSX_STOP_PRICE,";
        orderTopic = orderTopic + "EMSX_STRATEGY_END_TIME,";
        orderTopic = orderTopic + "EMSX_STRATEGY_PART_RATE1,";
        orderTopic = orderTopic + "EMSX_STRATEGY_PART_RATE2,";
        orderTopic = orderTopic + "EMSX_STRATEGY_START_TIME,";
        orderTopic = orderTopic + "EMSX_STRATEGY_STYLE,";
        orderTopic = orderTopic + "EMSX_STRATEGY_TYPE,";
        orderTopic = orderTopic + "EMSX_TICKER,";
        orderTopic = orderTopic + "EMSX_TIF,";
        orderTopic = orderTopic + "EMSX_TIME_STAMP,";
        orderTopic = orderTopic + "EMSX_TRAD_UUID,";
        orderTopic = orderTopic + "EMSX_TRADE_DESK,";
        orderTopic = orderTopic + "EMSX_TRADER,";
        orderTopic = orderTopic + "EMSX_TRADER_NOTES,";
        orderTopic = orderTopic + "EMSX_TS_ORDNUM,";
        orderTopic = orderTopic + "EMSX_TYPE,";
        orderTopic = orderTopic + "EMSX_UNDERLYING_TICKER,";
        orderTopic = orderTopic + "EMSX_USER_COMM_AMOUNT,";
        orderTopic = orderTopic + "EMSX_USER_COMM_RATE,";
        orderTopic = orderTopic + "EMSX_USER_FEES,";
        orderTopic = orderTopic + "EMSX_USER_NET_MONEY,";
        orderTopic = orderTopic + "EMSX_WORK_PRICE,";
        orderTopic = orderTopic + "EMSX_WORKING,";
        orderTopic = orderTopic + "EMSX_YELLOW_KEY";

        // We define a correlation ID that allows us to identify the original
        // request when we examine the events. This is useful in situations where
        // the same event handler is used the process messages for the Order and Route 
        // subscriptions, as well as request/response requests.
        orderSubscriptionID = new CorrelationID("");
        
        orderSubscription = new Subscription(orderTopic,orderSubscriptionID);
        System.out.println("Order Topic: " + orderTopic);
        SubscriptionList subscriptions = new SubscriptionList();
        subscriptions.add(orderSubscription);

        try {
            session.subscribe(subscriptions);
        } catch (Exception ex) {
            System.err.println("Failed to create Order subscription");
        }
        
    }
	
	private void createRouteSubscription(Session session)
    {
        System.out.println("Create Route subscription");

        // Create the topic string for the route subscription. Here, we are subscribing 
        // to every available route field, however, you can subscribe to only the fields
        // required for your application.
        String routeTopic = d_service + "/route?fields=";
        
        routeTopic = routeTopic + "API_SEQ_NUM,";
        routeTopic = routeTopic + "EMSX_ACCOUNT,";
        routeTopic = routeTopic + "EMSX_AMOUNT,";
        routeTopic = routeTopic + "EMSX_APA_MIC,";
        routeTopic = routeTopic + "EMSX_AVG_PRICE,";
        routeTopic = routeTopic + "EMSX_BROKER,";
        routeTopic = routeTopic + "EMSX_BROKER_COMM,";
        routeTopic = routeTopic + "EMSX_BROKER_LEI,";
        routeTopic = routeTopic + "EMSX_BROKER_SI,";
        routeTopic = routeTopic + "EMSX_BSE_AVG_PRICE,";
        routeTopic = routeTopic + "EMSX_BSE_FILLED,";
        routeTopic = routeTopic + "EMSX_BUYSIDE_LEI,";
        routeTopic = routeTopic + "EMSX_CLEARING_ACCOUNT,";
        routeTopic = routeTopic + "EMSX_CLEARING_FIRM,";
        routeTopic = routeTopic + "EMSX_CLIENT_IDENTIFICATION,";
        routeTopic = routeTopic + "EMSX_COMM_DIFF_FLAG,";
        routeTopic = routeTopic + "EMSX_COMM_RATE,";
        routeTopic = routeTopic + "EMSX_CURRENCY_PAIR,";
        routeTopic = routeTopic + "EMSX_CUSTOM_ACCOUNT,";
        routeTopic = routeTopic + "EMSX_DAY_AVG_PRICE,";
        routeTopic = routeTopic + "EMSX_DAY_FILL,";
        routeTopic = routeTopic + "EMSX_EXCHANGE_DESTINATION,";
        routeTopic = routeTopic + "EMSX_EXEC_INSTRUCTION,";
        routeTopic = routeTopic + "EMSX_EXECUTE_BROKER,";
        routeTopic = routeTopic + "EMSX_FILL_ID,";
        routeTopic = routeTopic + "EMSX_FILLED,";
        routeTopic = routeTopic + "EMSX_GPI,";
        routeTopic = routeTopic + "EMSX_GTD_DATE,";
        routeTopic = routeTopic + "EMSX_HAND_INSTRUCTION,";
        routeTopic = routeTopic + "EMSX_IS_MANUAL_ROUTE,";
        routeTopic = routeTopic + "EMSX_LAST_CAPACITY,";
        routeTopic = routeTopic + "EMSX_LAST_FILL_DATE,";
        routeTopic = routeTopic + "EMSX_LAST_FILL_TIME,";
        routeTopic = routeTopic + "EMSX_LAST_FILL_TIME_MICROSEC,";
        routeTopic = routeTopic + "EMSX_LAST_MARKET,";
        routeTopic = routeTopic + "EMSX_LAST_PRICE,";
        routeTopic = routeTopic + "EMSX_LAST_SHARES,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_DATE_ADDED,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_PRICE,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_SEQ_NO,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_SHARES,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_SIDE,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_TICKER,";
        routeTopic = routeTopic + "EMSX_LEG_FILL_TIME_ADDED,";
        routeTopic = routeTopic + "EMSX_LIMIT_PRICE,";
        routeTopic = routeTopic + "EMSX_MIFID_II_INSTRUCTION,";
        routeTopic = routeTopic + "EMSX_MISC_FEES,";
        routeTopic = routeTopic + "EMSX_ML_ID,";
        routeTopic = routeTopic + "EMSX_ML_LEG_QUANTITY,";
        routeTopic = routeTopic + "EMSX_ML_NUM_LEGS,";
        routeTopic = routeTopic + "EMSX_ML_PERCENT_FILLED,";
        routeTopic = routeTopic + "EMSX_ML_RATIO,";
        routeTopic = routeTopic + "EMSX_ML_REMAIN_BALANCE,";
        routeTopic = routeTopic + "EMSX_ML_STRATEGY,";
        routeTopic = routeTopic + "EMSX_ML_TOTAL_QUANTITY,";
        routeTopic = routeTopic + "EMSX_NOTES,";
        routeTopic = routeTopic + "EMSX_NSE_AVG_PRICE,";
        routeTopic = routeTopic + "EMSX_NSE_FILLED,";
        routeTopic = routeTopic + "EMSX_ORDER_TYPE,";
        routeTopic = routeTopic + "EMSX_OTC_FLAG,";
        routeTopic = routeTopic + "EMSX_P_A,";
        routeTopic = routeTopic + "EMSX_PERCENT_REMAIN,";
        routeTopic = routeTopic + "EMSX_PRINCIPAL,";
        routeTopic = routeTopic + "EMSX_QUEUED_DATE,";
        routeTopic = routeTopic + "EMSX_QUEUED_TIME,";
        routeTopic = routeTopic + "EMSX_QUEUED_TIME_MICROSEC,";
        routeTopic = routeTopic + "EMSX_REASON_CODE,";
        routeTopic = routeTopic + "EMSX_REASON_DESC,";
        routeTopic = routeTopic + "EMSX_REMAIN_BALANCE,";
        routeTopic = routeTopic + "EMSX_ROUTE_AS_OF_DATE,";
        routeTopic = routeTopic + "EMSX_ROUTE_AS_OF_TIME_MICROSEC,";
        routeTopic = routeTopic + "EMSX_ROUTE_CREATE_DATE,";
        routeTopic = routeTopic + "EMSX_ROUTE_CREATE_TIME,";
        routeTopic = routeTopic + "EMSX_ROUTE_CREATE_TIME_MICROSEC,";
        routeTopic = routeTopic + "EMSX_ROUTE_ID,";
        routeTopic = routeTopic + "EMSX_ROUTE_LAST_UPDATE_TIME,";
        routeTopic = routeTopic + "EMSX_ROUTE_LAST_UPDATE_TIME_MICROSEC,";
        routeTopic = routeTopic + "EMSX_ROUTE_PRICE,";
        routeTopic = routeTopic + "EMSX_ROUTE_REF_ID,";
        routeTopic = routeTopic + "EMSX_SEQUENCE,";
        routeTopic = routeTopic + "EMSX_SETTLE_AMOUNT,";
        routeTopic = routeTopic + "EMSX_SETTLE_DATE,";
        routeTopic = routeTopic + "EMSX_STATUS,";
        routeTopic = routeTopic + "EMSX_STOP_PRICE,";
        routeTopic = routeTopic + "EMSX_STRATEGY_END_TIME,";
        routeTopic = routeTopic + "EMSX_STRATEGY_PART_RATE1,";
        routeTopic = routeTopic + "EMSX_STRATEGY_PART_RATE2,";
        routeTopic = routeTopic + "EMSX_STRATEGY_START_TIME,";
        routeTopic = routeTopic + "EMSX_STRATEGY_STYLE,";
        routeTopic = routeTopic + "EMSX_STRATEGY_TYPE,";
        routeTopic = routeTopic + "EMSX_TIF,";
        routeTopic = routeTopic + "EMSX_TIME_STAMP,";
        routeTopic = routeTopic + "EMSX_TIME_STAMP_MICROSEC,";
        routeTopic = routeTopic + "EMSX_TRADE_REPORTING_INDICATOR,";
        routeTopic = routeTopic + "EMSX_TRANSACTION_REPORTING_MIC,";
        routeTopic = routeTopic + "EMSX_TYPE,";
        routeTopic = routeTopic + "EMSX_URGENCY_LEVEL,";
        routeTopic = routeTopic + "EMSX_USER_COMM_AMOUNT,";
        routeTopic = routeTopic + "EMSX_USER_COMM_RATE,";
        routeTopic = routeTopic + "EMSX_USER_FEES,";
        routeTopic = routeTopic + "EMSX_USER_NET_MONEY,";
        routeTopic = routeTopic + "EMSX_WAIVER_FLAG,";
        routeTopic = routeTopic + "EMSX_WORKING";

        
        // We define a correlation ID that allows us to identify the original
        // request when we examine the responses. This is useful in situations where
        // the same event handler is used the process messages for the Order and Route 
        // subscriptions, as well as request/response requests.
        routeSubscriptionID = new CorrelationID("");
        
        routeSubscription = new Subscription(routeTopic,routeSubscriptionID);
        System.out.println("Route Topic: " + routeTopic);
        SubscriptionList subscriptions = new SubscriptionList();
        subscriptions.add(routeSubscription);

        try {
            session.subscribe(subscriptions);
        } catch (Exception ex) {
            log.error("Failed to create Route subscription");
        }
    }
	
	private OrdemEmsxRequest buildOrderEmsx(Message msg) {
		OrdemEmsxRequest ordemEMSX = new OrdemEmsxRequest();
		
		if(msg.hasElement("EMSX_ORD_REF_ID") && !msg.getElementAsString("EMSX_ORD_REF_ID").isEmpty()) {
			ordemEMSX.setEmsxOrdRefId(Integer.parseInt(msg.getElementAsString("EMSX_ORD_REF_ID")));
		}else {
			ordemEMSX.setEmsxOrdRefId(null);
		}
    	ordemEMSX.setApiSeqNum(msg.hasElement("API_SEQ_NUM") ? msg.getElementAsInt64("API_SEQ_NUM") : null);
    	ordemEMSX.setEmsxAccount(msg.hasElement("EMSX_ACCOUNT") ? msg.getElementAsString("EMSX_ACCOUNT") : null);
    	ordemEMSX.setEmsxAmount(msg.hasElement("EMSX_AMOUNT") ? msg.getElementAsInt32("EMSX_AMOUNT") : null);
    	ordemEMSX.setEmsxArrivalPrice(msg.hasElement("EMSX_ARRIVAL_PRICE") ? msg.getElementAsFloat64("EMSX_ARRIVAL_PRICE") : null);
    	ordemEMSX.setEmsxAssetClass(msg.hasElement("EMSX_ASSET_CLASS") ? msg.getElementAsString("EMSX_ASSET_CLASS") : null);
    	ordemEMSX.setEmsxAssignedTrader(msg.hasElement("EMSX_ASSIGNED_TRADER") ? msg.getElementAsString("EMSX_ASSIGNED_TRADER") : null);
    	ordemEMSX.setEmsxAvgPrice(msg.hasElement("EMSX_AVG_PRICE") ? msg.getElementAsFloat64("EMSX_AVG_PRICE") : null);
    	ordemEMSX.setEmsxBasketName(msg.hasElement("EMSX_BASKET_NAME") ? msg.getElementAsString("EMSX_BASKET_NAME") : null);
    	ordemEMSX.setEmsxBasketNum(msg.hasElement("EMSX_BASKET_NUM") ? msg.getElementAsInt32("EMSX_BASKET_NUM") : null);
    	ordemEMSX.setEmsxBlockId(msg.hasElement("EMSX_BLOCK_ID") ? msg.getElementAsString("EMSX_BLOCK_ID") : null);
    	ordemEMSX.setEmsxBroker(msg.hasElement("EMSX_BROKER") ? msg.getElementAsString("EMSX_BROKER") : null);
    	ordemEMSX.setEmsxBrokerComm(msg.hasElement("EMSX_BROKER_COMM") ? msg.getElementAsFloat64("EMSX_BROKER_COMM") : null);
    	ordemEMSX.setEmsxBseAvgPrice(msg.hasElement("EMSX_BSE_AVG_PRICE") ? msg.getElementAsFloat64("EMSX_BSE_AVG_PRICE") : null);
    	ordemEMSX.setEmsxBseFilled(msg.hasElement("EMSX_BSE_FILLED") ? msg.getElementAsInt32("EMSX_BSE_FILLED") : null);
    	ordemEMSX.setEmsxBuysideLei(msg.hasElement("EMSX_BUYSIDE_LEI") ? msg.getElementAsString("EMSX_BUYSIDE_LEI") : null);
    	ordemEMSX.setEmsxClearingAccount(msg.hasElement("EMSX_CLEARING_ACCOUNT") ? msg.getElementAsString("EMSX_CLEARING_ACCOUNT") : null);
    	ordemEMSX.setEmsxClientIdentification(msg.hasElement("EMSX_CLIENT_IDENTIFICATION") ? msg.getElementAsString("EMSX_CLIENT_IDENTIFICATION") : null);
    	ordemEMSX.setEmsxCfdFlag(msg.hasElement("EMSX_CFD_FLAG") ? msg.getElementAsString("EMSX_CFD_FLAG") : null);
    	ordemEMSX.setEmsxCommDiffFlag(msg.hasElement("EMSX_COMM_DIFF_FLAG") ? msg.getElementAsString("EMSX_COMM_DIFF_FLAG") : null);
    	ordemEMSX.setEmsxCommRate(msg.hasElement("EMSX_COMM_RATE") ? msg.getElementAsFloat64("EMSX_COMM_RATE") : null);
    	ordemEMSX.setEmsxCurrencyPair(msg.hasElement("EMSX_CURRENCY_PAIR") ? msg.getElementAsString("EMSX_CURRENCY_PAIR") : null);
    	ordemEMSX.setEmsxCustomAccount(msg.hasElement("EMSX_CUSTOM_ACCOUNT") ? msg.getElementAsString("EMSX_CUSTOM_ACCOUNT") : null);
    	ordemEMSX.setEmsxDate(msg.hasElement("EMSX_DATE") ? msg.getElementAsInt32("EMSX_DATE") : null);
    	ordemEMSX.setEmsxDayAvgPrice(msg.hasElement("EMSX_DAY_AVG_PRICE") ? msg.getElementAsFloat64("EMSX_DAY_AVG_PRICE") : null);
    	ordemEMSX.setEmsxDayFill(msg.hasElement("EMSX_DAY_FILL") ? msg.getElementAsInt32("EMSX_DAY_FILL") : null);
    	ordemEMSX.setEmsxExchangeDestination(msg.hasElement("EMSX_EXCHANGE_DESTINATION") ? msg.getElementAsString("EMSX_EXCHANGE_DESTINATION") : null);
    	ordemEMSX.setEmsxExecInstruction(msg.hasElement("EMSX_EXEC_INSTRUCTION") ? msg.getElementAsString("EMSX_EXEC_INSTRUCTION") : null);
    	ordemEMSX.setEmsxExecuteBroker(msg.hasElement("EMSX_EXECUTE_BROKER") ? msg.getElementAsString("EMSX_EXECUTE_BROKER") : null);
    	ordemEMSX.setEmsxFillId(msg.hasElement("EMSX_FILL_ID") ? msg.getElementAsInt32("EMSX_FILL_ID") : null);
    	ordemEMSX.setEmsxFilled(msg.hasElement("EMSX_FILLED") ? msg.getElementAsInt32("EMSX_FILLED") : null);
    	ordemEMSX.setEmsxGpi(msg.hasElement("EMSX_GPI") ? msg.getElementAsString("EMSX_GPI") : null);
    	ordemEMSX.setEmsxGtdDate(msg.hasElement("EMSX_GTD_DATE") ? msg.getElementAsInt32("EMSX_GTD_DATE") : null);
    	ordemEMSX.setEmsxHandInstruction(msg.hasElement("EMSX_HAND_INSTRUCTION") ? msg.getElementAsString("EMSX_HAND_INSTRUCTION") : null);
    	ordemEMSX.setEmsxIsManualRoute(msg.hasElement("EMSX_IS_MANUAL_ROUTE") ? msg.getElementAsInt32("EMSX_IS_MANUAL_ROUTE") : null);
    	ordemEMSX.setEmsxIdleAmount(msg.hasElement("EMSX_IDLE_AMOUNT") ? msg.getElementAsInt32("EMSX_IDLE_AMOUNT") : null);    	
    	ordemEMSX.setEmsxInvestorId(msg.hasElement("EMSX_INVESTOR_ID") ? msg.getElementAsString("EMSX_INVESTOR_ID") : null);
    	ordemEMSX.setEmsxIsin(msg.hasElement("EMSX_ISIN") ? msg.getElementAsString("EMSX_ISIN") : null);
    	ordemEMSX.setEmsxLimitPrice(msg.hasElement("EMSX_LIMIT_PRICE") ? msg.getElementAsFloat64("EMSX_LIMIT_PRICE") : null);
    	ordemEMSX.setEmsxMifidIiInstruction(msg.hasElement("EMSX_MIFID_II_INSTRUCTION") ? msg.getElementAsString("EMSX_MIFID_II_INSTRUCTION") : null);
    	ordemEMSX.setEmsxModPendStatus(msg.hasElement("EMSX_MOD_PEND_STATUS") ? msg.getElementAsString("EMSX_MOD_PEND_STATUS") : null);
    	ordemEMSX.setEmsxNotes(msg.hasElement("EMSX_NOTES") ? msg.getElementAsString("EMSX_NOTES") : null);
    	ordemEMSX.setEmsxNseAvgPrice(msg.hasElement("EMSX_NSE_AVG_PRICE") ? msg.getElementAsFloat64("EMSX_NSE_AVG_PRICE") : null);
    	ordemEMSX.setEmsxNseFilled(msg.hasElement("EMSX_NSE_FILLED") ? msg.getElementAsInt32("EMSX_NSE_FILLED") : null);
    	ordemEMSX.setEmsxOrderAsOfDate(msg.hasElement("EMSX_ORDER_AS_OF_DATE") ? msg.getElementAsInt32("EMSX_ORDER_AS_OF_DATE") : null);
    	ordemEMSX.setEmsxOrderAsOfTimeMicrosec(msg.hasElement("EMSX_ORDER_AS_OF_TIME_MICROSEC") ? msg.getElementAsFloat64("EMSX_ORDER_AS_OF_TIME_MICROSEC") : null);
    	ordemEMSX.setEmsxOrderType(msg.hasElement("EMSX_ORDER_TYPE") ? msg.getElementAsString("EMSX_ORDER_TYPE") : null);
    	ordemEMSX.setEmsxOriginateTrader(msg.hasElement("EMSX_ORIGINATE_TRADER") ? msg.getElementAsString("EMSX_ORIGINATE_TRADER") : null);
    	ordemEMSX.setEmsxOriginateTraderFirm(msg.hasElement("EMSX_ORIGINATE_TRADER_FIRM") ? msg.getElementAsString("EMSX_ORIGINATE_TRADER_FIRM") : null);
    	ordemEMSX.setEmsxPercentRemain(msg.hasElement("EMSX_PERCENT_REMAIN") ? msg.getElementAsFloat64("EMSX_PERCENT_REMAIN") : null);
    	ordemEMSX.setEmsxPmUuid(msg.hasElement("EMSX_PM_UUID") ? msg.getElementAsInt32("EMSX_PM_UUID") : null);
    	ordemEMSX.setEmsxPortMgr(msg.hasElement("EMSX_PORT_MGR") ? msg.getElementAsString("EMSX_PORT_MGR") : null);
    	ordemEMSX.setEmsxPortName(msg.hasElement("EMSX_PORT_NAME") ? msg.getElementAsString("EMSX_PORT_NAME") : null);
    	ordemEMSX.setEmsxPortNum(msg.hasElement("EMSX_PORT_NUM") ? msg.getElementAsInt32("EMSX_PORT_NUM") : null);
    	ordemEMSX.setEmsxPosition(msg.hasElement("EMSX_POSITION") ? msg.getElementAsString("EMSX_POSITION") : null);
    	ordemEMSX.setEmsxPrinciple(msg.hasElement("EMSX_PRINCIPAL") ? msg.getElementAsFloat64("EMSX_PRINCIPAL") : null);
    	ordemEMSX.setEmsxProduct(msg.hasElement("EMSX_PRODUCT") ? msg.getElementAsString("EMSX_PRODUCT") : null);
    	ordemEMSX.setEmsxQueuedDate(msg.hasElement("EMSX_QUEUED_DATE") ? msg.getElementAsInt32("EMSX_QUEUED_DATE") : null);
    	ordemEMSX.setEmsxQueuedTime(msg.hasElement("EMSX_QUEUED_TIME") ? msg.getElementAsInt32("EMSX_QUEUED_TIME") : null);
    	ordemEMSX.setEmsxQueuedTimeMicrosec(msg.hasElement("EMSX_QUEUED_TIME_MICROSEC") ? msg.getElementAsFloat64("EMSX_QUEUED_TIME_MICROSEC") : null);
    	ordemEMSX.setEmsxReasonCode(msg.hasElement("EMSX_REASON_CODE") ? msg.getElementAsString("EMSX_REASON_CODE") : null);
    	ordemEMSX.setEmsxReasonDesc(msg.hasElement("EMSX_REASON_DESC") ? msg.getElementAsString("EMSX_REASON_DESC") : null);
    	ordemEMSX.setEmsxRemainBalance(msg.hasElement("EMSX_REMAIN_BALANCE") ? msg.getElementAsFloat64("EMSX_REMAIN_BALANCE") : null);
    	ordemEMSX.setEmsxRouteId(msg.hasElement("EMSX_ROUTE_ID") ? msg.getElementAsInt32("EMSX_ROUTE_ID") : null);
    	ordemEMSX.setEmsxRoutePrice(msg.hasElement("EMSX_ROUTE_PRICE") ? msg.getElementAsFloat64("EMSX_ROUTE_PRICE") : null);
    	ordemEMSX.setEmsxSecName(msg.hasElement("EMSX_SEC_NAME") ? msg.getElementAsString("EMSX_SEC_NAME") : null);
    	ordemEMSX.setEmsxSedol(msg.hasElement("EMSX_SEDOL") ? msg.getElementAsString("EMSX_SEDOL") : null);
    	ordemEMSX.setEmsxSequence(msg.hasElement("EMSX_SEQUENCE") ? msg.getElementAsInt32("EMSX_SEQUENCE") : null);
    	ordemEMSX.setEmsxSettleAmount(msg.hasElement("EMSX_SETTLE_AMOUNT") ? msg.getElementAsFloat64("EMSX_SETTLE_AMOUNT") : null);
    	ordemEMSX.setEmsxSettleDate(msg.hasElement("EMSX_SETTLE_DATE") ? msg.getElementAsInt32("EMSX_SETTLE_DATE") : null);
    	ordemEMSX.setEmsxSi(msg.hasElement("EMSX_SI") ? msg.getElementAsString("EMSX_SI") : null);
    	ordemEMSX.setEmsxSide(msg.hasElement("EMSX_SIDE") ? msg.getElementAsString("EMSX_SIDE") : null);
    	ordemEMSX.setEmsxStartAmount(msg.hasElement("EMSX_START_AMOUNT") ? msg.getElementAsInt32("EMSX_START_AMOUNT") : null);
    	ordemEMSX.setEmsxStatus(msg.hasElement("EMSX_STATUS") ? msg.getElementAsString("EMSX_STATUS") : null);
    	ordemEMSX.setEmsxAccount(msg.hasElement("EMSX_STEP_OUT_BROKER") ? msg.getElementAsString("EMSX_STEP_OUT_BROKER") : null);
    	ordemEMSX.setEmsxStopPrice(msg.hasElement("EMSX_STOP_PRICE") ? msg.getElementAsFloat64("EMSX_STOP_PRICE") : null);
    	ordemEMSX.setEmsxStrategyEndTime(msg.hasElement("EMSX_STRATEGY_END_TIME") ? msg.getElementAsInt32("EMSX_STRATEGY_END_TIME") : null);
    	ordemEMSX.setEmsxStrategyPartRate1(msg.hasElement("EMSX_STRATEGY_PART_RATE1") ? msg.getElementAsFloat64("EMSX_STRATEGY_PART_RATE1") : null);
    	ordemEMSX.setEmsxStrategyPartRate2(msg.hasElement("EMSX_STRATEGY_PART_RATE2") ? msg.getElementAsFloat64("EMSX_STRATEGY_PART_RATE2") : null);
    	ordemEMSX.setEmsxStrategyStartTime(msg.hasElement("EMSX_STRATEGY_START_TIME") ? msg.getElementAsInt32("EMSX_STRATEGY_START_TIME") : null);
    	ordemEMSX.setEmsxStrategyStyle(msg.hasElement("EMSX_STRATEGY_STYLE") ? msg.getElementAsString("EMSX_STRATEGY_STYLE") : null);
    	ordemEMSX.setEmsxStrategyType(msg.hasElement("EMSX_STRATEGY_TYPE") ? msg.getElementAsString("EMSX_STRATEGY_TYPE") : null);
    	ordemEMSX.setEmsxTicker(msg.hasElement("EMSX_TICKER") ? msg.getElementAsString("EMSX_TICKER") : null);
    	ordemEMSX.setEmsxTif(msg.hasElement("EMSX_TIF") ? msg.getElementAsString("EMSX_TIF") : null);
    	ordemEMSX.setEmsxTimeStamp(msg.hasElement("EMSX_TIME_STAMP") ? msg.getElementAsInt32("EMSX_TIME_STAMP") : null);
    	ordemEMSX.setEmsxTimeStampMicrosec(msg.hasElement("EMSX_TIME_STAMP_MICROSEC") ? msg.getElementAsFloat64("EMSX_TIME_STAMP_MICROSEC") : null);
    	ordemEMSX.setEmsxTradUuid(msg.hasElement("EMSX_TRAD_UUID") ? msg.getElementAsInt32("EMSX_TRAD_UUID") : null);
    	ordemEMSX.setEmsxTrader(msg.hasElement("EMSX_TRADER") ? msg.getElementAsString("EMSX_TRADER") : null);
    	ordemEMSX.setEmsxTraderNotes(msg.hasElement("EMSX_TRADER_NOTES") ? msg.getElementAsString("EMSX_TRADER_NOTES") : null);
    	ordemEMSX.setEmsxTsOrdnum(msg.hasElement("EMSX_TS_ORDNUM") ? msg.getElementAsInt32("EMSX_TS_ORDNUM") : null);
    	ordemEMSX.setEmsxType(msg.hasElement("EMSX_TYPE") ? msg.getElementAsString("EMSX_TYPE") : null);
    	ordemEMSX.setEmsxUnderlyingTicker(msg.hasElement("EMSX_UNDERLYING_TICKER") ? msg.getElementAsString("EMSX_UNDERLYING_TICKER") : null);
    	ordemEMSX.setEmsxUserCommAmount(msg.hasElement("EMSX_USER_COMM_AMOUNT") ? msg.getElementAsFloat64("EMSX_USER_COMM_AMOUNT") : null);
    	ordemEMSX.setEmsxUserCommRate(msg.hasElement("EMSX_USER_COMM_RATE") ? msg.getElementAsFloat64("EMSX_USER_COMM_RATE") : null);
    	ordemEMSX.setEmsxUserFees(msg.hasElement("EMSX_USER_FEES") ? msg.getElementAsFloat64("EMSX_USER_FEES") : null);
    	ordemEMSX.setEmsxUserNetMoney(msg.hasElement("EMSX_USER_NET_MONEY") ? msg.getElementAsFloat64("EMSX_USER_NET_MONEY") : null);
    	ordemEMSX.setEmsxWorking(msg.hasElement("EMSX_WORKING") ? msg.getElementAsInt32("EMSX_WORKING") : null);
    	ordemEMSX.setEmsxYellowKey(msg.hasElement("EMSX_YELLOW_KEY") ? msg.getElementAsString("EMSX_YELLOW_KEY") : null);
    	ordemEMSX.setEmsxAccount(msg.hasElement("EMSX_ACCOUNT") ? msg.getElementAsString("EMSX_ACCOUNT") : null);
    	ordemEMSX.setEmsxAccount(msg.hasElement("EMSX_ACCOUNT") ? msg.getElementAsString("EMSX_ACCOUNT") : null);
    	ordemEMSX.setEmsxAccount(msg.hasElement("EMSX_ACCOUNT") ? msg.getElementAsString("EMSX_ACCOUNT") : null);
		return ordemEMSX;
	}
	
}
