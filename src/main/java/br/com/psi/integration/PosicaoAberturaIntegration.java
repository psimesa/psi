package br.com.psi.integration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import lombok.extern.slf4j.Slf4j;

//@Configuration
//@EnableIntegration
@Slf4j
public class PosicaoAberturaIntegration {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
    @Value("${exchange.lote45}")
    private String exchange;
    
    @Value("${routeKey.lote45.opening-position}")
    private String routeOpeningPosition;

	private final String file = "C:\\Users\\INEX\\Documents\\Porto Seguro Investimentos\\Lote45\\POSICAO_ABERTURA_IMPORT.xlsx";
	
	@Bean
	public MessageChannel fileChannel() {
		return new DirectChannel();
	}

	@Bean
	@InboundChannelAdapter(value = "in", poller = @Poller(fixedDelay = "50000"))
	public MessageSource<String> source() {
		return new MessageSource<String>() {
			public Message<String> receive() {
				return MessageBuilder.withPayload(file).build();
			}
		};
	}

	@Transformer(inputChannel = "in", outputChannel = "out")
	public String transform(String message) {
		try {
			this.convertFile(message);
			return String.format("Posição de Abertura do Fundo atualizada com sucesso.");
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return String.format("ERRO - Não foi possível inserir, Posição de Abertura do Fundo.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return String.format("ERRO - Não foi possível inserir, Posição de Abertura do Fundo.");
		}
	}

	@ServiceActivator(inputChannel = "out")
	public void handler(String message) {
		log.info(message);
	}
	
	// Ler o arquivo de posição de abertura do Lote 45
	public Boolean convertFile(String file) throws IOException, InvalidFormatException {
		FileInputStream inputStream = new FileInputStream(file);
        Workbook workbook = WorkbookFactory.create(inputStream);
     // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 1. You can obtain a rowIterator and columnIterator and iterate over them
        Iterator<Row> rowIterator = sheet.rowIterator();
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            // Now let's iterate over the columns of the current row
            Iterator<Cell> cellIterator = row.cellIterator();
            ArrayList<String> attributes = new ArrayList<>();
            
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String cellValue = dataFormatter.formatCellValue(cell);
                attributes.add(cellValue);
            }
            
            rabbitTemplate.convertAndSend(exchange, routeOpeningPosition, attributes);
        }

        // Closing the workbook
        workbook.close();
		return true;
	}

}