package br.com.psi.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.entity.util.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Lucas Monteiro.
 * @since 07/02/2020.
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegraEnquadramento extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String descricao;
	
	private LocalDate dataInicio;
	
	private LocalDate dataFim;
	
	private Integer tipo;
	
	private Integer grupoBase;
	
	private Integer grupoCriterio;
	
	private Integer grupoRegra;
	
	private Integer subTipoRegra;
	
	private BigDecimal minimo;
	
	private BigDecimal maximo;
	
	private Integer numeroDias;

}
