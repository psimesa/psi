package br.com.psi.entity.util;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Data;

/**
 * Instancia uma nova BaseEntity(entidade) usada por todas as outras.
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@MappedSuperclass
public class BaseEntity {

	@Column(updatable = true, insertable = true)
	private LocalDateTime dataUltimaAlteracao;
	
	@Column(updatable = false, insertable = true)
	private LocalDateTime dataInclusao;
	
}

