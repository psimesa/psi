package br.com.psi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.entity.util.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Lucas Monteiro.
 * @since 17/03/2020.
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemEmsx extends BaseEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
   
   @NotNull
   @Column(name="emsxOrdRefId", insertable = true, updatable = false)
   private Integer emsxOrdRefId;
   
   @NotNull
   private Integer emsxAmount;
   
   @NotNull
   private String emsxHandInstruction;
   
   @NotNull
   private String emsxOrderType;
   
   @NotNull
   @Column(name="emsxSide", insertable = true, updatable = false)
   private String emsxSide;
   
   @NotNull
   private String emsxTicker;
   
   @NotNull
   private String emsxTif;
   
   private Long apiSeqNum;
   
   private String emsxAccount;
   
   private Double emsxArrivalPrice;
   
   private String emsxAssetClass;
   
   private String emsxAssignedTrader;
   
   private Double emsxAvgPrice;
   
   private String emsxBasketName;
   
   private Integer emsxBasketNum;
   
   private String emsxBlockId;
   
   private String emsxBroker;
   
   private Double emsxBrokerComm;
   
   private Double emsxBseAvgPrice;
   
   private Integer emsxBseFilled;
   
   private String emsxBuysideLei;
   
   private String emsxClientIdentification;
   
   private String emsxClearingAccount;
   
   private String emsxCfdFlag;
   
   private String emsxCommDiffFlag;
   
   private Double emsxCommRate;
   
   private String emsxCurrencyPair;
   
   private String emsxCustomAccount;
   
   private Integer emsxDate;
   
   private Double emsxDayAvgPrice;
   
   private Integer emsxDayFill;
   
   private String emsxDirBrokerFlag;
   
   private String emsxExchange;
   
   private String emsxExchangeDestination;
   
   private String emsxExecInstruction;
   
   private String emsxExecuteBroker;
   
   private Integer emsxFillId;
   
   private Integer emsxFilled;
   
   private String emsxGpi;
   
   private Integer emsxGtdDate;
   
   private Integer emsxIsManualRoute;
   
   private Integer emsxIdleAmount;
   
   private String emsxInvestorId;
   
   private String emsxIsin;
   
   private Double emsxLimitPrice;
   
   private String emsxMifidIiInstruction;
   
   private String emsxModPendStatus;
   
   private String emsxNotes;
   
   private Double emsxNseAvgPrice;
   
   private Integer emsxNseFilled;
   
   private Integer emsxOrderAsOfDate;
   
   private Double emsxOrderAsOfTimeMicrosec;

   private String emsxOriginateTrader;
   
   private String emsxOriginateTraderFirm;
   
   private Double emsxPercentRemain;
   
   private Integer emsxPmUuid;
   
   private String emsxPortMgr;
   
   private String emsxPortName;
   
   private Integer emsxPortNum;
   
   private String emsxPosition;
   
   private Double emsxPrinciple;
   
   private String emsxProduct;
   
   private Integer emsxQueuedDate;
   
   private Integer emsxQueuedTime;
   
   private Double emsxQueuedTimeMicrosec;
   
   private String emsxReasonCode;
   
   private String emsxReasonDesc;
   
   private Double emsxRemainBalance;
   
   private Integer emsxRouteId;
   
   private Double emsxRoutePrice;
   
   private String emsxSecName;
   
   private String emsxSedol;
   
   private Integer emsxSequence;
   
   private Double emsxSettleAmount;
   
   private Integer emsxSettleDate;
   
   private String emsxSi;
   
   private Integer emsxStartAmount;
   
   private String emsxStatus;
   
   private String emsxStepOutBroker;
   
   private Double emsxStopPrice;
   
   private Integer emsxStrategyEndTime;
   
   private Double emsxStrategyPartRate1;
   
   private Double emsxStrategyPartRate2;
   
   private Integer emsxStrategyStartTime;
   
   private String emsxStrategyStyle;
   
   private String emsxStrategyType;
   
   private Integer emsxTimeStamp;
   
   private Double emsxTimeStampMicrosec;
   
   private Integer emsxTradUuid;
   
   private String emsxTradeDesk;
   
   private String emsxTrader;
   
   private String emsxTraderNotes;
   
   private Integer emsxTsOrdnum;
   
   private String emsxType;
   
   private String emsxUnderlyingTicker;
   
   private Double emsxUserCommAmount;
   
   private Double emsxUserCommRate;
   
   private Double emsxUserFees;
   
   private Double emsxUserNetMoney;
   
   private Double emsxWorkPrice;
   
   private Integer emsxWorking;
   
   private String emsxYellowKey;
	
}