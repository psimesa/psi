package br.com.psi.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
public class Message {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String from;
	
	private String text;
	
}
