package br.com.psi.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.entity.util.BaseEntity;
import br.com.psi.enums.Status;
import br.com.psi.enums.TipoOrdem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ordem extends BaseEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
   
   private Integer contaMaster;
	
   private Integer fundo;
	
   private Integer cotaFundoInvestimento;
	
	@Enumerated(EnumType.ORDINAL)
	private TipoOrdem tipoOperacaoOrdem;
	
   private Integer tipoExecucaoOrdem;
	
   private BigDecimal limite;

   private Integer qtdCompra;
	
   private BigDecimal qtdCompraPercent;
	
   private Integer qtdVenda;
	
   private BigDecimal qtdVendaPercent;
	
   private Integer tickerCompra;
	
   private Integer tickerVenda;
	
   private String justificativa;
   
   @Column(updatable = false, insertable = true)
   private LocalDate dataInclusaoFilter;
   
   @Enumerated(EnumType.ORDINAL)
   private Status status;
	
}
