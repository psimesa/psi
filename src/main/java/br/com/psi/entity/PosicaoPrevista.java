package br.com.psi.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.entity.util.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Lucas Monteiro.
 * @since 27/02/2020.
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PosicaoPrevista extends BaseEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   private Integer idOrdemDetail;
   
   private Integer fundo;
   
   private Integer product;
   
   private Integer book;
   
   private BigDecimal financialPu;
   
   private Integer amount;
   
   private BigDecimal lastFinancialPu;

   private Integer lastAmount;
   
}
