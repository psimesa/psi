package br.com.psi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@AllArgsConstructor
public class OutputMessage {
	
	private String from;
	
	private String text;
	
	private String time;

}
