package br.com.psi.exception.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.psi.dto.response.error.ErrorResponse;
import br.com.psi.exception.PsiException;
import br.com.psi.exception.error.Error;


@RestControllerAdvice
public class PsiExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Object[] NO_PLACEHOLDERS = null;

  private final MessageSource messageSource;
  private final Locale locale;

  @Autowired
  public PsiExceptionHandler(MessageSource messageSource) {
    this.messageSource = messageSource;
    this.locale = LocaleContextHolder.getLocale();
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    List<Error> errors = this.getErrors(ex);
    ErrorResponse errorResponse = this.getErrorResponse(status, errors);
    return new ResponseEntity<Object>(errorResponse, status);
  }

  @ResponseBody
  @ExceptionHandler(PsiException.class)
  public ResponseEntity<ErrorResponse> handlerSpringBootIntoWasException(PsiException ex) {

    List<Error> errors = new ArrayList<Error>();
    Error error = new Error(ex.getMessage());
    errors.add(error);

    HttpStatus httpStatus = ex.getHttpStatus() != null ? ex.getHttpStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setMessage(ex.getMessage());
    errorResponse.setCode(httpStatus.value());
    errorResponse.setErrors(errors);
    errorResponse.setStatus(httpStatus.getReasonPhrase());

    return new ResponseEntity<ErrorResponse>(errorResponse, httpStatus);
  }

  private ErrorResponse getErrorResponse(HttpStatus httpStatus, List<Error> errors) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setCode(httpStatus.value());
    errorResponse.setMessage(this.messageSource.getMessage("testeeeee", NO_PLACEHOLDERS, this.locale));
    errorResponse.setErrors(errors);
    return errorResponse;
  }

  private List<Error> getErrors(MethodArgumentNotValidException ex) {

    List<ObjectError> allErrors = this.extractErrors(ex);

    if (CollectionUtils.isEmpty(allErrors)) {
      return Collections.<Error>emptyList();
    }

    List<Error> errors = new ArrayList<Error>();

    for (ObjectError oe : allErrors) {
      Error error = new Error(oe.getDefaultMessage(), oe.getObjectName(), oe.getCode());
      errors.add(error);
    }

    return errors;
  }

  private List<ObjectError> extractErrors(MethodArgumentNotValidException ex) {

    if (ex == null || ex.getBindingResult() == null || CollectionUtils.isEmpty(ex.getBindingResult().getAllErrors())) {
      return Collections.<ObjectError>emptyList();
    }

    return ex.getBindingResult().getAllErrors();
  }
}
