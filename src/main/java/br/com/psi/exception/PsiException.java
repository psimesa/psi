package br.com.psi.exception;

import org.springframework.http.HttpStatus;

public class PsiException extends RuntimeException {

  private HttpStatus httpStatus;
  private String message;
  private Throwable cause;

  private PsiException(PsiExceptionBuilder builder) {
    this.httpStatus = builder.httpStatus;
    this.message = builder.message;
    this.cause = builder.cause;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public Throwable getCause() {
    return cause;
  }

  public static class PsiExceptionBuilder {
    private final String message;
    private final Throwable cause;
    private HttpStatus httpStatus;

    public PsiExceptionBuilder(String message, Throwable cause) {
      this.message = message;
      this.cause = cause;
    }

    public PsiExceptionBuilder httpStatus(HttpStatus httpStatus) {
      this.httpStatus = httpStatus;
      return this;
    }

    public PsiException build() {
      return new PsiException(this);
    }
  }
}
