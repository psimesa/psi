package br.com.psi.exception.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Error {

  @JsonProperty("mensagem")
  private final String message;

  @JsonProperty("campo")
  private final String field;

  @JsonProperty("parametro")
  private final Object parameter;

  public Error(String message) {
    this.message = message;
    this.field = "";
    this.parameter = "";
  }

  public Error(String message, String field, Object parameter) {
    this.message = message;
    this.field = field;
    this.parameter = parameter;
  }

  public String getMessage() {
    return message;
  }

  public String getField() {
    return field;
  }

  public Object getParameter() {
    return parameter;
  }
}