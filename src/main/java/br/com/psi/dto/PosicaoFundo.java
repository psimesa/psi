package br.com.psi.dto;

import java.math.BigDecimal;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.psi.enums.TypePosition;
import lombok.Data;

@Data
public class PosicaoFundo {
	
	private Integer id;

	private Integer fund;
	
	private Integer product;
	
	private Integer idOrdemDetail;
	
	private Integer book;
	
	private BigDecimal financialPu;
	
	private Integer Amount;
	
	@Enumerated(EnumType.STRING)
	private TypePosition type;

}
