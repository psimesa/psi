package br.com.psi.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AcaoDTO {
	
	private String fundo;
	
	private String cotaFundoInvestimento;
	
	private String tipoOperacaoOrdem;
	
	private String tipoExecucaoOrdem;
	
	private BigDecimal limite;

	private Integer qtdCompra;
	
	private BigDecimal qtdCompraPercent;
	
	private Integer qtdVenda;
	
	private BigDecimal qtdVendaPercent;
	
	private Integer tickerCompra;
	
	private Integer tickerVenda;
	
	private String justificativa;

}
