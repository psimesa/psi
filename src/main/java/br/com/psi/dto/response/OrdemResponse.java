package br.com.psi.dto.response;

import java.math.BigDecimal;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import br.com.psi.enums.Status;
import br.com.psi.enums.TipoOrdem;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemResponse extends AbstractResponse{
	
	private Integer fundo;
	
	private Integer contaMaster;
	
	private Integer cotaFundoInvestimento;
	
	@Enumerated(EnumType.ORDINAL)
	private TipoOrdem tipoOperacaoOrdem;
	
	private Integer tipoExecucaoOrdem;
	
	private BigDecimal limite;

	private Integer qtdCompra;
	
	private BigDecimal qtdCompraPercent;
	
	private Integer qtdVenda;
	
	private BigDecimal qtdVendaPercent;
	
	private Integer tickerCompra;
	
	private Integer tickerVenda;
	
	private String justificativa;
	
	private Status status;
	
}
