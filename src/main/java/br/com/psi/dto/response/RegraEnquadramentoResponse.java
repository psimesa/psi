package br.com.psi.dto.response;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegraEnquadramentoResponse extends AbstractResponse{
	
	private String descricao;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate dataInicio;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate dataFim;	
	
	private Integer tipo;
	
	private Integer grupoBase;
	
	private Integer grupoCriterio;
	
	private Integer grupoRegra;
	
	private Integer subTipoRegra;
	
	private BigDecimal minimo;
	
	private BigDecimal maximo;
	
	private Integer numeroDias;
	
}
