package br.com.psi.dto.response;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import br.com.psi.enums.Status;
import br.com.psi.enums.TypeOrderDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemDetailResponse extends AbstractResponse{
	   
	private Integer idOrdem;
	   
	private Integer contaMaster;
		
	private Integer fundo;
		
	private Integer book;
	
	private Integer ticker;

	@Enumerated(EnumType.STRING)
	private TypeOrderDetail type;
		
	private Integer amount;
		
	private BigDecimal financialPu;
		
	@Column(updatable = false, insertable = true)
	private LocalDate date;
	   
	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
//	private Integer solicitante;

//	private Integer gerente;
	
}
