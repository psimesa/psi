package br.com.psi.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndiceBovespaResponse extends AbstractResponse{

   private String empresa;

   private String ticker;
   
   private String tipo;
   
   private Integer quantidade;
   
   private BigDecimal participacao;
	
}
