package br.com.psi.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import br.com.psi.enums.TypeRebalance;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RebalanceResponse extends AbstractResponse{

	   private TypeRebalance type;
	   
	   private Integer fundo;
	   
	   private BigDecimal price;
	
}
