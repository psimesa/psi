package br.com.psi.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PosicaoAberturaResponse extends AbstractResponse{

	private String fund;
	
	private String product;
	
	private String book;
	
	private BigDecimal financialPu;
	
	private Integer Amount;
	
    private BigDecimal lastFinancialPu;
	
    private Integer lastAmount;

}
