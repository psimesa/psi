package br.com.psi.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoExecucaoOrdemResponse extends AbstractResponse{

	private String tipo;
	
}
