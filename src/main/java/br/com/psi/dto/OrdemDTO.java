package br.com.psi.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemDTO extends AbstractResponse{

	private String contaMaster;
	
	private List<AcaoDTO> acoes;
	
}
