package br.com.psi.dto.util;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
public abstract class AbstractResponse {
	
	/** O id. */
	private Integer id;
	
	/** Data da última alteração. */
	private LocalDateTime dataUltimaAlteracao;
	
	/** Data de inclusão. */
	private LocalDateTime dataInclusao;

}
