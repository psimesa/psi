package br.com.psi.dto.util;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lucas Monteiro.
 * @since 27/01/2020.
 */
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractRequest {
	
	private Integer id;
	
	private LocalDateTime dataUltimaAlteracao;

	private LocalDateTime dataInclusao;
	
}
