package br.com.psi.dto.request;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FundoRequest extends AbstractRequest{

	private String fundo;
	
	@NotNull
	private Integer contaMaster;
	
	private String codigoCarteiraInvestimento;
	
}
