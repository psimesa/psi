package br.com.psi.dto.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PosicaoAberturaRequest extends AbstractRequest{

	private Integer fund;
	
	private Integer product;
	
	private Integer book;
	
	private BigDecimal financialPu;
	
	private Integer Amount;
	
}
