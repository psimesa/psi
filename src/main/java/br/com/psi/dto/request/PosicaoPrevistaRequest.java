package br.com.psi.dto.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PosicaoPrevistaRequest extends AbstractRequest{

	   private Integer idOrdemDetail;
	   
	   private Integer fundo;
	   
	   private Integer product;
	   
	   private Integer book;
	   
	   private BigDecimal financialPu;
	   
	   private Integer amount;
	   
	   private BigDecimal lastFinancialPu;

	   private Integer lastAmount;
	   
}
