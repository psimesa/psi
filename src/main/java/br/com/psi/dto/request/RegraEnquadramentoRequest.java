package br.com.psi.dto.request;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegraEnquadramentoRequest extends AbstractRequest{

	private String descricao;
	
	private LocalDate dataInicio;

	private LocalDate dataFim;
	
	private Integer tipo;
	
	private Integer grupoBase;
	
	private Integer grupoCriterio;
	
	private Integer grupoRegra;
	
	private Integer subTipoRegra;
	
	private BigDecimal minimo;
	
	private BigDecimal maximo;
	
	private Integer numeroDias;
	
}
