package br.com.psi.dto.request;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import br.com.psi.enums.Status;
import br.com.psi.enums.TipoOrdem;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemRequest extends AbstractRequest{
	
	@NotNull
	private Integer fundo;

	@NotNull
	private Integer contaMaster;
	
	@NotNull
	private Integer cotaFundoInvestimento;
	
	@NotNull
	@NotBlank
	@Enumerated(EnumType.ORDINAL)
	private TipoOrdem tipoOperacaoOrdem;
	
	private Integer tipoExecucaoOrdem;
	
	private BigDecimal limite;

	private Integer qtdCompra;
	
	private BigDecimal qtdCompraPercent;
	
	private Integer qtdVenda;
	
	private BigDecimal qtdVendaPercent;
	
	private Integer tickerCompra;
	
	private Integer tickerVenda;
	
	private String justificativa;
	
	@NotNull
	private Status status;
	
	private LocalDate dataInclusaoFilter;
	
//	private Integer solicitante;

//	private Integer gerente;
	
}
