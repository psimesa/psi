package br.com.psi.dto.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import br.com.psi.enums.TypeRebalance;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RebalanceRequest extends AbstractRequest{

	   @NotNull
	   private TypeRebalance type;
	   
	   @NotNull
	   private Integer fundo;
	   
	   private BigDecimal price;
	
}
