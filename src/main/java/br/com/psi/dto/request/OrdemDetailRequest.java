package br.com.psi.dto.request;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import br.com.psi.enums.Status;
import br.com.psi.enums.TypeOrderDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdemDetailRequest extends AbstractRequest{
	   
	private Integer idOrdem;
	   
	private Integer contaMaster;
		
	private Integer fundo;
		
	private Integer book;
	
	private Integer ticker;

	@Enumerated(EnumType.STRING)
	private TypeOrderDetail type;
		
	private Integer amount;
		
	private BigDecimal financialPu;
		
	@Column(updatable = false, insertable = true)
	private LocalDate date;
	   
	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
//	private Integer solicitante;

//	private Integer gerente;
	
}
