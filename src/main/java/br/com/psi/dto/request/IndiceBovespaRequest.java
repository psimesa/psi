package br.com.psi.dto.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.psi.dto.util.AbstractRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndiceBovespaRequest extends AbstractRequest{

   private String empresa;

   private String ticker;
   
   private String tipo;
   
   private Integer quantidade;
   
   private BigDecimal participacao;
	
}
