package br.com.psi.bo.response;

import br.com.psi.enums.Status;
import lombok.Data;

@Data
public class OrdemBO {
	
	private Integer id;
	
	private String fundo;

	private Integer qtdCompra;
	
	private Integer qtdVenda;
	
	private Integer execucao;
	
	private String contaMaster;
	
	private Status status;
	
	private Integer type;
	
}
