package br.com.psi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.dto.request.FundoRequest;
import br.com.psi.dto.response.FundoResponse;
import br.com.psi.service.FundoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Fundos de Investimento")
@CrossOrigin
@RequestMapping("/fundo/")
public class FundoController {
	
	@Autowired
	private FundoService service;

	/**
	 * Retorna Fundos de Investimento.
	 * @return FundoResponse.
	 * 
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todos Fundos de Investimentos")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<FundoResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Fundo de Investimento.
	 * @param FundoRequest.
	 * @return FundoResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar um novo Fundo de Investimento basta enviar os dados para essa API, e todas as validações serão realizadas.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<FundoResponse> insert(@RequestBody FundoRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar Fundo.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "Api responsável por deletar um fundo.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar Fundo.
	 * @param FundoRequest .
	 * @return FundoResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por atualizar um fundo de investimento.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<FundoResponse> update(@RequestBody FundoRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar Fundo pelo ID.
	 * @param Integer id.
	 * @return FundoResponse.
	 * 
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "Se desejar consultar Fundo por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<FundoResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}
	
}
