package br.com.psi.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.bo.response.OrdemBO;
import br.com.psi.dto.request.OrdemRequest;
import br.com.psi.dto.response.OrdemResponse;
import br.com.psi.enums.Status;
import br.com.psi.service.OrdemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Ordem")
@CrossOrigin
@RequestMapping("/ordem/")
public class OrdemController {
	
	@Autowired
	private OrdemService service;

	/**
	 * Retorna Ordens cadastradas no sistema.
	 * @return ContaMasterResponse.
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todas ordens cadastradas no sistema.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<OrdemResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Ordem.
	 * @param OrdemRequest .
	 * @return OrdemResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar uma nova Ordem basta enviar os dados para essa API, e todas as validações serão realizadas.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<OrdemResponse> insert(@RequestBody OrdemRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar Ordem.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("delete")
	@ApiOperation(value = "Api responsável por deletar um Ordem.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar Ordem.
	 * @param OrdemRequest .
	 * @return OrdemResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por atualizar um Ordem.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<OrdemResponse> update(@RequestBody OrdemRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar Ordem pelo ID.
	 * @param Integer id.
	 * @return OrdemResponse.
	 * 
	 */
	@GetMapping("getById")
	@ApiOperation(value = "Se desejar consultar Ordem por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<OrdemResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}

	/**
	 * Retorna Ordens cadastradas no sistema pelo filtro informado.
	 * @return ContaMasterResponse.
	 */
	@GetMapping("filter")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todas ordens cadastradas no sistema pelo filtro informado.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<OrdemBO>> getFiltered(@RequestParam List<Status> status, @RequestParam @DateTimeFormat(iso = ISO.DATE, pattern = "dd/MM/yyyy") LocalDate data){
		return ResponseEntity.ok(this.service.getFiltered(status, data));
	}
	
}
