package br.com.psi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.dto.request.GrupoEnquadramentoRequest;
import br.com.psi.dto.response.GrupoEnquadramentoResponse;
import br.com.psi.service.GrupoEnquadramentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Grupo de Enquadramento")
@CrossOrigin
@RequestMapping("/enquadramento-grupo/")
public class GrupoEnquadramentoController {
	
	@Autowired
	private GrupoEnquadramentoService service;

	/**
	 * Retorna Grupos de Enquadramento cadastrados no sistema.
	 * @return GrupoEnquadramentoResponse.
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todos os Grupos de Enquadramento cadastrados no sistema.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<GrupoEnquadramentoResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Grupo Enquadramento.
	 * @param GrupoEnquadramentoRequest .
	 * @return GrupoEnquadramentoResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar um novo Grupo de Enquadramento basta enviar os dados para essa API, e todas as validações serão realizadas.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<GrupoEnquadramentoResponse> insert(@RequestBody GrupoEnquadramentoRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar GrupoEnquadramento.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "Api responsável por deletar um Grupo de Enquadramento.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@PathVariable Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar GrupoEnquadramento.
	 * @param GrupoEnquadramentoRequest .
	 * @return GrupoEnquadramentoResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por atualizar um Grupo de Enquadramento.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<GrupoEnquadramentoResponse> update(@RequestBody GrupoEnquadramentoRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar GrupoEnquadramento pelo ID.
	 * @param Integer id.
	 * @return GrupoEnquadramentoResponse.
	 * 
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "Se desejar consultar Grupo de Enquadramento por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<GrupoEnquadramentoResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}

}
