package br.com.psi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.dto.request.RegraEnquadramentoRequest;
import br.com.psi.dto.response.RegraEnquadramentoResponse;
import br.com.psi.service.RegraEnquadramentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Regra de Enquadramento")
@CrossOrigin
@RequestMapping("/enquadramento-regra/")
public class RegraEnquadramentoController {
	
	@Autowired
	private RegraEnquadramentoService service;

	/**
	 * Retorna Regras de Enquadramento cadastradas.
	 * @return RegraEnquadramentoResponse.
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todas as Regras de Enquadramento cadastradas no sistema.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<RegraEnquadramentoResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Regra Enquadramento.
	 * @param RegraEnquadramentoRequest .
	 * @return RegraEnquadramentoResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar uma nova Regra de Enquadramento basta enviar os dados para essa API, e todas as validações serão realizadas.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<RegraEnquadramentoResponse> insert(@RequestBody RegraEnquadramentoRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar Regra Enquadramento.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "Api responsável por deletar uma Regra de Enquadramento.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@PathVariable Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar Regra Enquadramento.
	 * @param RegraEnquadramentoRequest .
	 * @return RegraEnquadramentoResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por atualizar uma Regra de Enquadramento.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<RegraEnquadramentoResponse> update(@RequestBody RegraEnquadramentoRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar RegraEnquadramento pelo ID.
	 * @param Integer id.
	 * @return RegraEnquadramentoResponse.
	 * 
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "Se desejar consultar Regra de Enquadramento por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<RegraEnquadramentoResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}

}
