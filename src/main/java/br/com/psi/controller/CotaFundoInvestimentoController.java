package br.com.psi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.dto.request.CotaFundoInvestimentoRequest;
import br.com.psi.dto.response.CotaFundoInvestimentoResponse;
import br.com.psi.service.CotaFundoInvestimentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Cota(BOOK)")
@CrossOrigin
@RequestMapping("/cota/")
public class CotaFundoInvestimentoController {
	
	@Autowired
	private CotaFundoInvestimentoService service;

	/**
	 * Retorna Cota de Fundos de Investimento.
	 * @return CotaFundoInvestimentoResponse.
	 * 
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todas Cotas de Fundos de Invesimento")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<CotaFundoInvestimentoResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Cota de Fundo de Invesimento.
	 * @param CotaFundoInvestimentoRequest.
	 * @return CotaFundoInvestimentoResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar uma nova Cota de Fundo de Invesimento basta enviar os dados para essa API, e todas as validações serão realizadas.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<CotaFundoInvestimentoResponse> insert(@RequestBody CotaFundoInvestimentoRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar Cota de Fundo de Invesimento.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "Api responsável por deletar um book.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar Cota de Fundo de Invesimento.
	 * @param CotaFundoInvestimentoRequest .
	 * @return CotaFundoInvestimentoResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por deletar um BOOK.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<CotaFundoInvestimentoResponse> update(@RequestBody CotaFundoInvestimentoRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar Cota de Fundo de Investimento pelo ID.
	 * @param Integer id.
	 * @return CotaFundoInvestimentoResponse.
	 * 
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "Se desejar consultar Cota de Fundo de Invesimento por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<CotaFundoInvestimentoResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}
	
	/**
	 * Buscar Cota de Fundo de Investimento pelo ID do Fundo.
	 * @param Integer idFundo.
	 * @return CotaFundoInvestimentoResponse.
	 * 
	 */
	@GetMapping("fundo")
	@ApiOperation(value = "Se desejar consultar Cota de Fundo de Invesimento por ID do Fundo.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<CotaFundoInvestimentoResponse>> getByIdFundo(@RequestParam Integer idFundo){
		return ResponseEntity.ok(this.service.getByIdFundo(idFundo));
	}

}
