package br.com.psi.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.psi.dto.request.FundoRequest;
import br.com.psi.dto.response.FundoResponse;
import br.com.psi.entity.Message;
import br.com.psi.entity.OutputMessage;
import br.com.psi.service.FundoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "API PSI")
@CrossOrigin
@RequestMapping("/")
public class PsiController {
	
	@Autowired
	private FundoService service;

	/**
	 * Retorna fundos de investimento.
	 * @param FundoRequest.
	 * @return FundoResponse.
	 * 
	 */
	@GetMapping("/fundo")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todos os fundos de investimentos cadastrados na base")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<FundoResponse>> obterFundos(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar fundos de investimento.
	 * @param FundoRequest .
	 * @return FundoResponse.
	 * 
	 */
	@PostMapping("/fundo")
	@ApiOperation(value = "Se desejar cadastrar um novo Fundo de Investimento basta enviar os dados para essa API, e todas as validações serão feitas para o novo Fundo.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<FundoResponse> registraFundo(@RequestBody FundoRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Upload do arquivo(csv).
	 * @param file arquivos de preços.
	 * @return A entidade em booleano.
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 * 
	 */
	@PostMapping("/upload")
	@ApiOperation(value = "Essa API é possivel inserir um excel com diversos fundos e fazer o upload para serem cadastrados.")
	public ResponseEntity<Boolean> handleFileUpload(@RequestParam MultipartFile file) throws InvalidFormatException, IOException {
		return ResponseEntity.ok(service.upload(file));
	}
	
	@MessageMapping("chat")
	@SendTo("topic/messages")
	public OutputMessage send(Message message) throws Exception {
	    String time = new SimpleDateFormat("HH:mm").format(new Date());
	    return new OutputMessage(message.getFrom(), message.getText(), time);
	}
	
}
