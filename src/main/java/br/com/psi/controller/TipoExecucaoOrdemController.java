package br.com.psi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.psi.dto.request.TipoExecucaoOrdemRequest;
import br.com.psi.dto.response.TipoExecucaoOrdemResponse;
import br.com.psi.service.TipoExecucaoOrdemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Tipo Execução Ordem")
@CrossOrigin
@RequestMapping("/tipo-execucao-ordem/")
public class TipoExecucaoOrdemController {
	
	@Autowired
	private TipoExecucaoOrdemService service;

	/**
	 * Retorna tipo execucao ordem de investimento.
	 * @return TipoExecucaoTipoExecucaoOrdemResponse.
	 * 
	 */
	@GetMapping("get-all")
	@ApiOperation(value = "Quando utilizar essa API irá retorna todos os tipos de execução de envio da ordem")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<List<TipoExecucaoOrdemResponse>> getAll(){
		return ResponseEntity.ok(this.service.getAll());
	}
	
	/**
	 * Gravar Tipo Execucao Ordem.
	 * @param TipoExecucaoTipoExecucaoOrdemRequest.
	 * @return TipoExecucaoTipoExecucaoOrdemResponse.
	 * 
	 */
	@PostMapping("insert")
	@ApiOperation(value = "Se desejar cadastrar um novo Tipo de Execução basta enviar os dados para essa API.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<TipoExecucaoOrdemResponse> insert(@RequestBody TipoExecucaoOrdemRequest req){
		return ResponseEntity.ok(this.service.insert(req));
	}
	
	/**
	 * Deletar Ordem.
	 * @param id .
	 * @return Boolean.
	 * 
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "Api responsável por deletar um Tipo Execucao Ordem.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<Boolean> delete(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.delete(id));
	}
	
	/**
	 * Atualizar Ordem.
	 * @param TipoExecucaoOrdemRequest .
	 * @return TipoExecucaoOrdemResponse.
	 * 
	 */
	@PutMapping("update")
	@ApiOperation(value = "Api responsável por atualizar um Tipo Execucao Ordem.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<TipoExecucaoOrdemResponse> update(@RequestBody TipoExecucaoOrdemRequest req){
		return ResponseEntity.ok(this.service.update(req));
	}
	
	/**
	 * Buscar Ordem pelo ID.
	 * @param Integer id.
	 * @return TipoExecucaoOrdemResponse.
	 * 
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "Se desejar consultar Tipo Execucao Ordem por ID.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<TipoExecucaoOrdemResponse> getById(@RequestParam Integer id){
		return ResponseEntity.ok(this.service.getById(id));
	}
	
}
