package br.com.psi.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.RegraEnquadramentoRequest;
import br.com.psi.dto.response.RegraEnquadramentoResponse;
import br.com.psi.entity.RegraEnquadramento;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.RegraEnquadramentoRepository;
import br.com.psi.service.RegraEnquadramentoService;
import br.com.psi.util.MethodsUtils;

@Service
public class RegraEnquadramentoServiceImpl implements RegraEnquadramentoService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private RegraEnquadramentoRepository repository;

	@Override
	public RegraEnquadramentoResponse insert(RegraEnquadramentoRequest req) {
		try {
			req.setDataInclusao(LocalDateTime.now());
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public List<RegraEnquadramentoResponse> getAll() {
		List<RegraEnquadramentoResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			RegraEnquadramentoResponse res = new RegraEnquadramentoResponse();
			list.add((RegraEnquadramentoResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public RegraEnquadramentoResponse getById(Integer id) {
		try {
			RegraEnquadramentoResponse res = new RegraEnquadramentoResponse();
			return (RegraEnquadramentoResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public RegraEnquadramentoResponse update(RegraEnquadramentoRequest req) {
		try {
			req.setDataUltimaAlteracao(LocalDateTime.now());
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private RegraEnquadramentoResponse save(RegraEnquadramentoRequest req){
		RegraEnquadramentoResponse res = new RegraEnquadramentoResponse();
		RegraEnquadramento ms = new RegraEnquadramento();
		ms = (RegraEnquadramento) this.utils.convertToEntity(req, ms);
		try {		
			res = (RegraEnquadramentoResponse) this.utils.convertToDto(this.repository.save(ms), res);
			this.utils.generateLogInfo(res);
			return res;
		}catch (Exception e) {
			this.utils.generateLogError(req);
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
}
