package br.com.psi.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoExecutadaRequest;
import br.com.psi.entity.PosicaoExecutada;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.PosicaoExecutadaRepository;
import br.com.psi.service.PosicaoExecutadaService;
import br.com.psi.util.MethodsUtils;

@Service
public class PosicaoExecutadaServiceImpl implements PosicaoExecutadaService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private PosicaoExecutadaRepository repository;

	@Override
	public PosicaoFundo insert(PosicaoExecutadaRequest req) {
		req.setDataInclusao(LocalDateTime.now());
		return this.save(req);
	}


	@Override
	public PosicaoFundo getByTickerAndBookAndFundo(String ticker, String book, String fundo) {
		return this.buildPosition(this.repository.findByProductAndBookAndFundo(ticker, book, fundo));
	}
	
	private PosicaoFundo save(PosicaoExecutadaRequest req){
		PosicaoExecutada entity = new PosicaoExecutada();
		return this.buildPosition(this.repository.save((PosicaoExecutada) this.utils.convertToEntity(req, entity)));
	}


	private PosicaoFundo buildPosition(PosicaoExecutada entity) {
		PosicaoFundo res = new PosicaoFundo();
		res.setAmount(entity.getAmount());
		res.setBook(entity.getBook());
		res.setFinancialPu(entity.getFinancialPu());
		res.setFund(entity.getFundo());
		res.setProduct(entity.getProduct());
		return res;
	}


	@Override
	@Transactional
	public Boolean deleteByIdOrdemDetail(Integer idOrdemDetail) {
		try {
			this.repository.deleteByIdOrdemDetail(idOrdemDetail);
			this.utils.generateLogInfo(idOrdemDetail);
			return true;
		} catch (Exception e) {
			this.utils.generateLogError(idOrdemDetail);
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

}
