package br.com.psi.service.impl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import br.com.psi.dto.request.FundoRequest;
import br.com.psi.dto.response.FundoResponse;
import br.com.psi.entity.Fundo;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.FundoRepository;
import br.com.psi.service.FundoService;
import br.com.psi.util.MethodsUtils;

@Service
public class FundoServiceImpl implements FundoService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private FundoRepository repository;
	
//	@Autowired
//	private KafkaTemplate<Object, FundoRequest> kafkaTemplate;

	@Override
	public List<FundoResponse> getAll() {
		List<FundoResponse> fundos = new ArrayList<>();
		FundoResponse fundo = new FundoResponse();
		this.repository.findAll().forEach(f -> {
			fundos.add((FundoResponse) this.utils.convertToDto(f, fundo));
		});
		return fundos;
	}

	@Override
	public FundoResponse insert(FundoRequest req) {
		FundoResponse fr = new FundoResponse();
		Fundo f = new Fundo();
		f = (Fundo) this.utils.convertToEntity(req, f);
		return (FundoResponse) this.utils.convertToDto(this.repository.save(f), fr);
	}

	@Override
	public Boolean upload(MultipartFile file) throws IOException, InvalidFormatException {
		InputStream inputStream =  new BufferedInputStream(file.getInputStream());
        Workbook workbook = WorkbookFactory.create(inputStream);
     // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 1. You can obtain a rowIterator and columnIterator and iterate over them
        Iterator<Row> rowIterator = sheet.rowIterator();
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            // Now let's iterate over the columns of the current row
            Iterator<Cell> cellIterator = row.cellIterator();
            ArrayList<String> attributes = new ArrayList<>();
            
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String cellValue = dataFormatter.formatCellValue(cell);
                attributes.add(cellValue);
            }
            
            FundoRequest f = new FundoRequest();
			//this.repository.save(f);
            //rabbitTemplate.convertAndSend(this.queue.getName(), f.toString());
        }

        // Closing the workbook
        workbook.close();
		return true;
	}
	
//	@KafkaListener(topics = "test-topic")
//	 @RabbitListener(queues = {"${queue.order.name}"})
	private void processPrices(@Payload String fundo) {
		Gson gson = new Gson();
		System.out.println(fundo);
//		FundoRequest fr = gson.fromJson(fundo, FundoRequest.class);
//		Fundo f = new Fundo();
//		f = (Fundo) this.utils.convertToEntity(fr, f);
//        this.repository.save(f);
	}

	@Override
	public FundoResponse getById(Integer id) {
		try {
			FundoResponse res = new FundoResponse();
			return (FundoResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public FundoResponse update(FundoRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	@Override
	public FundoResponse getByFundo(String name) {
		FundoResponse res = new FundoResponse();
		return (FundoResponse) this.utils.convertToDto(this.repository.findByFundo(name), res);
	}
	
	private FundoResponse save(FundoRequest req){
		FundoResponse res = new FundoResponse();
		Fundo ms = new Fundo();
		ms = (Fundo) this.utils.convertToEntity(req, ms);
		return (FundoResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}
	
}
