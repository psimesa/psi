package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.ContaMasterRequest;
import br.com.psi.dto.response.ContaMasterResponse;
import br.com.psi.entity.ContaMaster;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.ContaMasterRepository;
import br.com.psi.service.ContaMasterService;
import br.com.psi.util.MethodsUtils;

@Service
public class ContaMasterServiceImpl implements ContaMasterService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private ContaMasterRepository repository;

	@Override
	public ContaMasterResponse insert(ContaMasterRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public List<ContaMasterResponse> getAll() {
		List<ContaMasterResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			ContaMasterResponse res = new ContaMasterResponse();
			list.add((ContaMasterResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public ContaMasterResponse getById(Integer id) {
		try {
			ContaMasterResponse res = new ContaMasterResponse();
			return (ContaMasterResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public ContaMasterResponse update(ContaMasterRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}
	
	private ContaMasterResponse save(ContaMasterRequest req){
		ContaMasterResponse res = new ContaMasterResponse();
		ContaMaster ms = new ContaMaster();
		ms = (ContaMaster) this.utils.convertToEntity(req, ms);
		return (ContaMasterResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}
	
}
