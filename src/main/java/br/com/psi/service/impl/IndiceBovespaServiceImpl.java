package br.com.psi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.IndiceBovespaRequest;
import br.com.psi.dto.response.IndiceBovespaResponse;
import br.com.psi.entity.IndiceBovespa;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.IndiceBovespaRepository;
import br.com.psi.service.IndiceBovespaService;
import br.com.psi.util.MethodsUtils;

@Service
public class IndiceBovespaServiceImpl implements IndiceBovespaService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private IndiceBovespaRepository repository;

	@Override
	public IndiceBovespaResponse insert(IndiceBovespaRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public IndiceBovespaResponse getByTicker(String ticker) {
		try {
			return (IndiceBovespaResponse) this.utils.convertToDto(this.repository.findByTicker(ticker), new IndiceBovespaResponse());
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	private IndiceBovespaResponse save(IndiceBovespaRequest req){
		IndiceBovespa ms = (IndiceBovespa) this.utils.convertToEntity(req, new IndiceBovespa());
		return (IndiceBovespaResponse) this.utils.convertToDto(this.repository.save(ms), new IndiceBovespaResponse());
	}
	
}
