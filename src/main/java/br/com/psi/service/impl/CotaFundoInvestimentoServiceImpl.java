package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.CotaFundoInvestimentoRequest;
import br.com.psi.dto.response.CotaFundoInvestimentoResponse;
import br.com.psi.entity.CotaFundoInvestimento;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.CotaFundoInvestimentoRepository;
import br.com.psi.service.CotaFundoInvestimentoService;
import br.com.psi.util.MethodsUtils;

@Service
public class CotaFundoInvestimentoServiceImpl implements CotaFundoInvestimentoService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private CotaFundoInvestimentoRepository repository;

	@Override
	public CotaFundoInvestimentoResponse insert(CotaFundoInvestimentoRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public List<CotaFundoInvestimentoResponse> getAll() {
		try {
			List<CotaFundoInvestimentoResponse> list = new ArrayList<>();
			this.repository.findAll().forEach(c -> {
				CotaFundoInvestimentoResponse res = new CotaFundoInvestimentoResponse();
				list.add((CotaFundoInvestimentoResponse) this.utils.convertToDto(c, res));
			});
			return list;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public CotaFundoInvestimentoResponse getById(Integer id) {
		try {
			CotaFundoInvestimentoResponse res = new CotaFundoInvestimentoResponse();
			return (CotaFundoInvestimentoResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public CotaFundoInvestimentoResponse update(CotaFundoInvestimentoRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private CotaFundoInvestimentoResponse save(CotaFundoInvestimentoRequest req){
		CotaFundoInvestimentoResponse res = new CotaFundoInvestimentoResponse();
		CotaFundoInvestimento ms = new CotaFundoInvestimento();
		ms = (CotaFundoInvestimento) this.utils.convertToEntity(req, ms);
		return (CotaFundoInvestimentoResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}

	@Override
	public List<CotaFundoInvestimentoResponse> getByIdFundo(Integer idFundo) {
		try {
			List<CotaFundoInvestimentoResponse> newList = new ArrayList<>();
			List<CotaFundoInvestimento> list = this.repository.findByFundo(idFundo);
			list.forEach(c -> {
				CotaFundoInvestimentoResponse res = new CotaFundoInvestimentoResponse();
				newList.add((CotaFundoInvestimentoResponse) this.utils.convertToDto(c, res));
			});
			return newList;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public CotaFundoInvestimentoResponse getByName(String name) {
		CotaFundoInvestimentoResponse res = new CotaFundoInvestimentoResponse();
		return (CotaFundoInvestimentoResponse) this.utils.convertToDto(this.repository.findByNome(name), res);
	}
	
}
