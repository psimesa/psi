package br.com.psi.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.psi.bo.response.OrdemBO;
import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.OrdemDetailRequest;
import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.dto.request.OrdemRequest;
import br.com.psi.dto.request.PosicaoExecutadaRequest;
import br.com.psi.dto.request.PosicaoPrevistaRequest;
import br.com.psi.dto.response.ContaMasterResponse;
import br.com.psi.dto.response.FundoResponse;
import br.com.psi.dto.response.IndiceBovespaResponse;
import br.com.psi.dto.response.OrdemDetailResponse;
import br.com.psi.dto.response.OrdemEmsxResponse;
import br.com.psi.dto.response.OrdemResponse;
import br.com.psi.dto.response.TickerResponse;
import br.com.psi.entity.Ordem;
import br.com.psi.enums.ActionBloomberg;
import br.com.psi.enums.Status;
import br.com.psi.enums.TipoOrdem;
import br.com.psi.enums.TypeOrderDetail;
import br.com.psi.enums.TypePosition;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.OrdemRepository;
import br.com.psi.service.ContaMasterService;
import br.com.psi.service.FundoService;
import br.com.psi.service.IndiceBovespaService;
import br.com.psi.service.OrdemDetailService;
import br.com.psi.service.OrdemEmsxService;
import br.com.psi.service.OrdemService;
import br.com.psi.service.PosicaoAberturaService;
import br.com.psi.service.PosicaoExecutadaService;
import br.com.psi.service.PosicaoPrevistaService;
import br.com.psi.service.TickerService;
import br.com.psi.util.MethodsUtils;

@Service
public class OrdemServiceImpl implements OrdemService {

	@Autowired
	private MethodsUtils utils;
	
	@Autowired
	private PosicaoPrevistaService posicaoPrevistaService;
	
	@Autowired
	private PosicaoAberturaService posicaoAberturaService;
	
	@Autowired
	private PosicaoExecutadaService posicaoExecutadaService;

	@Autowired
	private OrdemRepository repository;
	
	@Autowired
	private OrdemEmsxService ordemEmsxService;
	
	@Autowired
	private IndiceBovespaService indiceBovespaService;
	
	@Autowired
	private TickerService tickerService;
	
	@Autowired
	private FundoService fundoService;

	@Autowired
	private ContaMasterService contaMasterService;
	
	@Autowired
	private OrdemDetailService ordemDetailService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
    @Value("${exchange.bloomberg}")
    private String exchange;
    
    @Value("${routeKey.bloomberg.order-bloomberg}")
    private String routeOrderBloomberg;
    
	@Override
	public OrdemResponse insert(OrdemRequest req) {
		return this.validOrderType(req);
	}

	@Override
	public List<OrdemResponse> getAll() {
		List<OrdemResponse> list = new ArrayList<>();
		this.repository.findAll(Sort.by(Sort.Direction.DESC, "id")).forEach(c -> {
			OrdemResponse res = new OrdemResponse();
			list.add((OrdemResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public OrdemResponse getById(Integer id) {
		try {
			OrdemResponse res = new OrdemResponse();
			return (OrdemResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public OrdemResponse update(OrdemRequest req) {
		return (OrdemResponse) this.utils.convertToDto(this.repository.save((Ordem) this.utils.convertToEntity(req, new Ordem())), new OrdemResponse());
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.ordemDetailService.deleteByIdOrdem(id);
			this.repository.deleteById(id);
			this.utils.generateLogInfo(id);
			return true;
		}catch (Exception e) {
			this.utils.generateLogError(id);
			throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	private OrdemResponse save(OrdemRequest req){
		Ordem ms = (Ordem) this.utils.convertToEntity(req, new Ordem());
		OrdemResponse res = (OrdemResponse) this.utils.convertToDto(this.repository.save(ms), new OrdemResponse());
		this.utils.generateLogInfo(res);
		return res;
	}

	@Override
	public List<OrdemBO> getFiltered(List<Status> status, LocalDate data) {
		List<OrdemBO> list = new ArrayList<>();
		this.repository.findByStatusInAndDataInclusaoFilter(status, data, Sort.by(Sort.Direction.DESC, "dataInclusao")).forEach(c -> {
			list.add(this.buildBO(c));
		});
		return list;
	}

	private OrdemBO buildBO(Ordem c) {
		OrdemBO or = new OrdemBO();
		or.setId(c.getId());
		or.setExecucao(null);
		or.setType(c.getTipoOperacaoOrdem().getTipo());
		or.setQtdCompra(c.getQtdCompra() == null ? null : c.getQtdCompra());
		or.setQtdVenda(c.getQtdVenda() == null ? null : c.getQtdVenda());
		FundoResponse f = this.fundoService.getById(c.getFundo());
		ContaMasterResponse cc = this.contaMasterService.getById(f.getContaMaster());
		or.setContaMaster(cc.getNome());
		or.setFundo(f.getFundo());
		or.setStatus(c.getStatus());
		return or;
	}
	
	private OrdemResponse validOrderType(OrdemRequest req) {
    	switch (req.getTipoOperacaoOrdem()) {
		case ZERAR:
			return this.reset(req);
		case EQUAL:
			return this.equal(req);
		case COMPRA:
			return this.buy(req);
		case VENDA:
			return this.sell(req);
		case LS:
			return this.longAndShort(req);
		case ZERAR_V_C:
			return this.resetSellAndBuy(req);
		case NEUTRALIZAR:
			return this.neutralize(req);
		default:
			return null;
		}
    }

	/** Tipo da ordem (Zerar), zera o ticker venda, com o valor retornado da venda comprar o ticker compra */
	private OrdemResponse reset(OrdemRequest req) {
		validationReset(req);
		PosicaoFundo position = this.getFundPosition(req);
		if(position == null) {
			throw new PsiException.PsiExceptionBuilder("Não existe posição para os dados infomados, por favor verificar.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		req.setQtdVenda(position.getAmount());
		OrdemResponse resOrdem = this.save(this.buildSellOrderRequest(req, position));
		OrdemDetailResponse detail = this.buildOrdemDetail(resOrdem, position);
		OrdemEmsxResponse resp = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detail));
		this.insertNewPosition(detail, position);
		resp.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(resp);
		return resOrdem;
	}
	
	/** Tipo da ordem (Compra) */
	private OrdemResponse buy(OrdemRequest req) {
		this.validOrderBuy(req);
		PosicaoFundo position = this.getFundPosition(req);
		OrdemResponse respOrdem = this.save(this.buildBuyOrderRequest(req, position));
		OrdemDetailResponse detail = this.buildOrdemDetail(respOrdem, position);
		OrdemEmsxResponse resp = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detail));
		this.insertNewPosition(detail, position);
		resp.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(resp);
		return respOrdem;
	}

	/** Tipo da ordem (Venda) */
	private OrdemResponse sell(OrdemRequest req) {
		this.validOrderSell(req);
		PosicaoFundo position = this.getFundPosition(req);
		if(position == null) {
			throw new PsiException.PsiExceptionBuilder("Não existe posição para os dados infomados, por favor verificar.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		OrdemResponse respOrdem = this.save(this.buildSellOrderRequest(req, position));
		OrdemDetailResponse detail = this.buildOrdemDetail(respOrdem, position);
		OrdemEmsxResponse resp = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detail));
		this.insertNewPosition(detail, position);
		resp.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(resp);
		return respOrdem;
	}
	
	private OrdemRequest build(OrdemRequest req) {
		OrdemRequest res = new OrdemRequest();
		res.setContaMaster(req.getContaMaster());
		res.setCotaFundoInvestimento(req.getCotaFundoInvestimento());
		res.setFundo(req.getFundo());
		res.setJustificativa(req.getJustificativa());
		res.setLimite(req.getLimite() == null ? null : req.getLimite());
		res.setQtdCompra(req.getQtdCompra() == null ? null : req.getQtdCompra());
		res.setQtdCompraPercent(req.getQtdCompraPercent() == null ? null : req.getQtdCompraPercent());
		res.setQtdVenda(req.getQtdVenda() == null ? null : req.getQtdVenda());
		res.setQtdVendaPercent(req.getQtdVendaPercent() == null ? null : req.getQtdVendaPercent());
		res.setTickerCompra(req.getTickerCompra() == null ? null : req.getTickerCompra());
		res.setTickerVenda(req.getTickerVenda() == null ? null : req.getTickerVenda());
		res.setTipoExecucaoOrdem(req.getTipoExecucaoOrdem() == null ? null : req.getTipoExecucaoOrdem());
		res.setTipoOperacaoOrdem(req.getTipoOperacaoOrdem() == null ? null : req.getTipoOperacaoOrdem());
		return res;
	}
	
	/** Tipo da ordem LS(Long And Short) */
	private OrdemResponse longAndShort(OrdemRequest req) {
		validLongAndShort(req);
		
		OrdemRequest reqSell = this.build(req);
		reqSell.setQtdCompra(null);
		reqSell.setQtdCompraPercent(null);
		reqSell.setTickerCompra(null);
		
		OrdemRequest reqBuy = this.build(req);
		reqBuy.setQtdVenda(null);
		reqBuy.setQtdVendaPercent(null);
		reqBuy.setTickerVenda(null);

		PosicaoFundo positionSell = this.getFundPosition(reqSell);
		if(positionSell == null) {
			throw new PsiException.PsiExceptionBuilder("Não existe posição para os dados informados de Venda.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		PosicaoFundo positionBuy = this.getFundPosition(reqBuy);

		OrdemResponse respOrdem = this.save(this.buildOrderRequest(req, positionSell, positionBuy));
		
		/** Cadastra e envia ordem de Venda */
		reqSell.setId(respOrdem.getId());
		reqSell.setQtdVenda(respOrdem.getQtdVenda());
		OrdemDetailResponse detailSell = this.buildOrdemDetailLS(reqSell, positionSell);
		OrdemEmsxResponse respSell = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detailSell));
		this.insertNewPosition(detailSell, positionSell);
		respSell.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(respSell);
		
		/** Cadastra e envia ordem de Compra */
		reqBuy.setId(respOrdem.getId());
		reqBuy.setQtdCompra(respOrdem.getQtdCompra());
		OrdemDetailResponse detailBuy = this.buildOrdemDetailLS(reqBuy, positionBuy);
		OrdemEmsxResponse respBuy = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detailBuy));
		this.insertNewPosition(detailBuy, positionBuy);
		respBuy.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(respBuy);
		
		return respOrdem;
	}
	
	
	private void validLongAndShort(OrdemRequest req) {
		if((req.getQtdCompra() == null && req.getQtdCompraPercent() == null) || req.getTickerCompra() == null) {
			throw new PsiException.PsiExceptionBuilder("Para esse tipo de ordem informar dados de Compra.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		if((req.getQtdVenda() == null && req.getQtdVendaPercent() == null) || req.getTickerVenda() == null) {
			throw new PsiException.PsiExceptionBuilder("Para esse tipo de ordem informar dados de Venda.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	/** Tipo da ordem  Zerar (Ticker Venda) comprando o financeiro no (Ticker Compra) */
	private OrdemResponse resetSellAndBuy(OrdemRequest req) {
		OrdemRequest reqReset = this.build(req);
		reqReset.setQtdCompra(null);
		reqReset.setQtdCompraPercent(null);
		reqReset.setTickerCompra(null);
		
		OrdemRequest reqBuy = this.build(req);
		reqBuy.setQtdVenda(null);
		reqBuy.setQtdVendaPercent(null);
		reqBuy.setTickerVenda(null);

		PosicaoFundo positionReset = this.getFundPosition(reqReset);
		PosicaoFundo positionBuy = this.getFundPosition(reqBuy);
		
		if(positionReset == null) {
			throw new PsiException.PsiExceptionBuilder("Não existe posição para os dados infomados, por favor verificar.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		reqReset.setQtdVenda(positionReset.getAmount());
		req.setQtdVenda(positionReset.getAmount());
		
		BigDecimal qtd = new BigDecimal(positionReset.getAmount()).multiply(positionReset.getFinancialPu()).setScale(2, RoundingMode.HALF_UP);
		double totalQtd = qtd.floatValue() / 39.67;
		req.setQtdCompra((int) (Math.round(totalQtd / 100 ) * 100));
		OrdemResponse res = this.save(this.buildOrderRequest(req, positionReset, positionBuy));
		reqReset.setId(res.getId());
		reqBuy.setId(res.getId());
		
		reqBuy.setQtdCompra(req.getQtdCompra());
		OrdemDetailResponse detailReset = this.buildOrdemDetailLS(reqReset, positionReset);
		OrdemEmsxResponse respReset = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detailReset));
		this.insertNewPosition(detailReset, positionReset);
		respReset.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(respReset);
		
		OrdemDetailResponse detailBuy = this.buildOrdemDetailLS(reqBuy, positionBuy);
		OrdemEmsxResponse respBuy = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detailBuy));
		this.insertNewPosition(detailBuy, positionBuy);
		respBuy.setAction(ActionBloomberg.CreateOrder);
		sendOrderToTerminal(respBuy);
		
		return res;
	}
	
	/** Tipo de ordem Neutralizar, usa o financeiro para efetuar compra ou a venda de outro ticker */
	private OrdemResponse neutralize(OrdemRequest req) {
		OrdemResponse res = new OrdemResponse();
		OrdemRequest reqBuy = this.build(req);
		reqBuy.setTickerVenda(null);
		
		OrdemRequest reqSell = this.build(req);
		reqSell.setQtdCompra(null);

		PosicaoFundo positionBuy = this.getFundPosition(reqBuy);
		PosicaoFundo positionSell = this.getFundPosition(reqSell);
		
		TickerResponse ticker = this.tickerService.getById(reqBuy.getTickerCompra());
		IndiceBovespaResponse indice = indiceBovespaService.getByTicker(ticker.getName());
		if(indice == null) {
			throw new PsiException.PsiExceptionBuilder("O Ticker de Compra enviado não faz parte do Indice Bovespa.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		if(indice.getQuantidade() > 0) {
			if(indice.getQuantidade() > (positionBuy == null ? 0 : positionBuy.getAmount())){
				req.setQtdCompra(indice.getQuantidade() - (positionBuy == null ? 0 : positionBuy.getAmount()));
				req.setQtdVenda(req.getQtdCompra());
				res = this.save(this.buildBuyOrderRequest(req, positionBuy));
				
				reqBuy.setQtdCompra(req.getQtdCompra());
				reqBuy.setId(res.getId());
				
				OrdemDetailResponse detail = this.buildOrdemDetailLS(reqBuy, positionBuy);
				OrdemEmsxResponse respBuy = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detail));
				this.insertNewPosition(detail, positionBuy);
				respBuy.setAction(ActionBloomberg.CreateOrder);
				sendOrderToTerminal(respBuy);

				reqSell.setQtdVenda(req.getQtdCompra());
				reqSell.setId(res.getId());
				OrdemDetailResponse detailSell = this.buildOrdemDetailLS(reqSell, positionSell);
				OrdemEmsxResponse respSell = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detailSell));
				this.insertNewPosition(detailSell, positionSell);
				respSell.setAction(ActionBloomberg.CreateOrder);
				sendOrderToTerminal(respSell);
				
			}else {
				req.setQtdVenda(indice.getQuantidade() - (positionBuy == null ? 0 : positionBuy.getAmount()));
				req.setQtdCompra(req.getQtdCompra());
				res = this.save(this.buildBuyOrderRequest(req, positionSell));
				
				OrdemDetailResponse detail = this.buildOrdemDetailLS(reqSell, positionSell);
				OrdemEmsxResponse respSell = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detail));
				this.insertNewPosition(detail, positionSell);
				respSell.setAction(ActionBloomberg.CreateOrder);
				sendOrderToTerminal(respSell);

				OrdemDetailResponse detailBuy = this.buildOrdemDetailLS(reqBuy, positionBuy);
				OrdemEmsxResponse respBuy = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detailBuy));
				this.insertNewPosition(detailBuy, positionBuy);
				respBuy.setAction(ActionBloomberg.CreateOrder);
				sendOrderToTerminal(respBuy);
			}
		}
		return res;
	}
	
	/** O ticker enviado será comparado ao indice Ibovespa e realizará a compra ou a venda para manter iguais */
	private OrdemResponse equal(OrdemRequest req) {
		PosicaoFundo position = this.getFundPosition(req);
		OrdemResponse res = new OrdemResponse();
		
		if(req.getTickerCompra() != null) {
			TickerResponse ticker = this.tickerService.getById(req.getTickerCompra());
			IndiceBovespaResponse indice = indiceBovespaService.getByTicker(ticker.getName());
			if(indice == null) {
				throw new PsiException.PsiExceptionBuilder("O Ticker de Compra enviado não faz parte do Indice Bovespa.", null)
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
			}
			
			if(indice.getQuantidade() > 0) {
				if(indice.getQuantidade() > (position == null ? 0 : position.getAmount())){
					req.setQtdCompra(indice.getQuantidade() - (position == null ? 0 : position.getAmount()));	
					res = this.save(this.buildBuyOrderRequest(req, position));
					OrdemDetailResponse detail = this.buildOrdemDetail(res, position);
					OrdemEmsxResponse resp = this.ordemEmsxService.insert(this.buildBuyOrderEmsxRequest(detail));
					this.insertNewPosition(detail, position);
					resp.setAction(ActionBloomberg.CreateOrder);
					sendOrderToTerminal(resp);
				}else {
					req.setQtdCompra(null);
					req.setTickerVenda(req.getTickerCompra());
					req.setTickerCompra(null);
					req.setQtdVenda(position.getAmount() - indice.getQuantidade());	
					res = this.save(this.buildSellOrderRequest(req, position));
					OrdemDetailResponse detail = this.buildOrdemDetail(res, position);
					OrdemEmsxResponse resp = this.ordemEmsxService.insert(this.buildSellOrderEmsxRequest(detail));
					this.insertNewPosition(detail, position);
					resp.setAction(ActionBloomberg.CreateOrder);
					sendOrderToTerminal(resp);
				}
			}
		}
		return res;
	}
	
	/** Valida dados da ordem de ZERAR */
	private void validationReset(OrdemRequest req) {
		/** Valida se está sendo passado algum dado de compra, o que não é permitido pelo tipo da ordem */
		if(req.getQtdCompra() != null || req.getQtdCompraPercent() != null || req.getTickerCompra() != null) {
			throw new PsiException.PsiExceptionBuilder("Para esse tipo de ordem não informar dados de compra.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		/** Valida as informações de venda */
		if(req.getTickerVenda() == null) {
			throw new PsiException.PsiExceptionBuilder("Por favor informar Ticker de venda.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	/** Valida dados da ordem de compra */
	private void validOrderBuy(OrdemRequest req){
		/** Valida se está sendo passado algum dado de compra, o que não é permitido pelo tipo da ordem */
		if(req.getQtdVenda() != null || req.getQtdVendaPercent() != null || req.getTickerVenda() != null) {
			throw new PsiException.PsiExceptionBuilder("Para esse tipo de ordem não informar dados de Venda.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		/** Valida as informações de venda */
		if(req.getTickerCompra() == null || (req.getQtdCompra() == null && req.getQtdCompraPercent() == null)) {
			throw new PsiException.PsiExceptionBuilder("Por favor informar Dados de Compra.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	/** Valida dados da ordem de compra */
	private void validOrderSell(OrdemRequest req){
		/** Valida se está sendo passado algum dado de compra, o que não é permitido pelo tipo da ordem */
		if(req.getQtdCompra() != null || req.getQtdCompraPercent() != null || req.getTickerCompra() != null) {
			throw new PsiException.PsiExceptionBuilder("Para esse tipo de ordem não informar dados de Compra.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
		
		/** Valida as informações de venda */
		if(req.getTickerVenda() == null || (req.getQtdVenda() == null && req.getQtdVendaPercent() == null)) {
			throw new PsiException.PsiExceptionBuilder("Por favor informar Dados de Venda.", null)
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	private OrdemDetailResponse buildOrdemDetailLS(OrdemRequest ordem, PosicaoFundo position) {
		OrdemDetailRequest req = new OrdemDetailRequest();
		req.setAmount(ordem.getQtdCompra() == null ? ordem.getQtdVenda() : ordem.getQtdCompra());
		req.setBook(ordem.getCotaFundoInvestimento());
		req.setContaMaster(ordem.getContaMaster());
		req.setDataInclusao(LocalDateTime.now());
		req.setDate(LocalDate.now());
		req.setFinancialPu(position == null ? null : position.getFinancialPu());
		req.setFundo(ordem.getFundo());
		req.setIdOrdem(ordem.getId());
		req.setStatus(Status.NEW);
		req.setTicker(ordem.getTickerCompra() == null ? ordem.getTickerVenda() : ordem.getTickerCompra());
		req.setType(ordem.getQtdCompra() == null ? TypeOrderDetail.SELL : TypeOrderDetail.BUY);
		return this.ordemDetailService.insert(req);
	}
	
	private OrdemDetailResponse buildOrdemDetail(OrdemResponse ordem, PosicaoFundo position) {
		OrdemDetailRequest req = new OrdemDetailRequest();
		req.setAmount(ordem.getQtdCompra() == null ? ordem.getQtdVenda() : ordem.getQtdCompra());
		req.setBook(ordem.getCotaFundoInvestimento());
		req.setContaMaster(ordem.getContaMaster());
		req.setDataInclusao(LocalDateTime.now());
		req.setDate(LocalDate.now());
		req.setFinancialPu(position == null ? null : position.getFinancialPu());
		req.setFundo(ordem.getFundo());
		req.setIdOrdem(ordem.getId());
		req.setStatus(Status.NEW);
		req.setTicker(ordem.getTickerCompra() == null ? ordem.getTickerVenda() : ordem.getTickerCompra());
		req.setType(ordem.getQtdCompra() == null ? TypeOrderDetail.SELL : TypeOrderDetail.BUY);
		return this.ordemDetailService.insert(req);
	}
	
	private void insertNewPosition(OrdemDetailResponse req, PosicaoFundo position) {
		PosicaoPrevistaRequest reqPrev = new PosicaoPrevistaRequest();

		if(req.getType().equals(TypeOrderDetail.BUY)) {
			reqPrev.setAmount((position == null ? 0 : position.getAmount()) + req.getAmount());
		}else {
			if((position == null ? 0 : position.getAmount()) > req.getAmount()) {
				reqPrev.setAmount((position == null ? 0 : position.getAmount()) - req.getAmount());
			}else {
				reqPrev.setAmount(req.getAmount() - (position == null ? 0 : position.getAmount()));
			}
		}
		reqPrev.setBook(req.getBook());
		reqPrev.setDataInclusao(LocalDateTime.now());
		reqPrev.setFundo(req.getFundo());
		reqPrev.setIdOrdemDetail(req.getId());
		reqPrev.setProduct(req.getTicker());
		reqPrev.setLastAmount(position == null ? null : position.getAmount());
		reqPrev.setLastFinancialPu(position == null ? null : position.getFinancialPu());
		/** Valor da unidade do papel */
		reqPrev.setFinancialPu(null);
		this.posicaoPrevistaService.insert(reqPrev);
		
		PosicaoExecutadaRequest reqExec = new PosicaoExecutadaRequest();
		reqExec.setAmount(0);
		reqExec.setBook(req.getBook());
		reqExec.setDataInclusao(LocalDateTime.now());
		reqExec.setFundo(req.getFundo());
		reqExec.setTotalAmount(req.getAmount());
		reqExec.setIdOrdemDetail(req.getId());
		reqExec.setLastAmount(position == null ? null : position.getAmount());
		reqExec.setLastFinancialPu(position == null ? null : position.getFinancialPu());
		reqExec.setProduct(req.getTicker());
		/** Valor da unidade do papel */
		reqExec.setFinancialPu(null);
		this.posicaoExecutadaService.insert(reqExec);
	}
	

	private void sendOrderToTerminal(OrdemEmsxResponse resp) {
		Gson gson = new Gson();
		rabbitTemplate.convertAndSend(exchange, routeOrderBloomberg, gson.toJson(resp));
	}
	
	private PosicaoFundo getFundPosition(OrdemRequest req) {
		PosicaoFundo p = new PosicaoFundo();
		p = this.posicaoPrevistaService.getByTickerAndBookAndFundo(req.getTickerCompra() == null ? req.getTickerVenda() : req.getTickerCompra(), req.getCotaFundoInvestimento(), req.getFundo());
		if(p == null) {
			p = this.posicaoAberturaService.getByTickerAndBookAndFundo(req.getTickerCompra() == null ? req.getTickerVenda() : req.getTickerCompra(), req.getCotaFundoInvestimento(), req.getFundo());
			if(p == null) {
				return null;
			}else {
				p.setType(TypePosition.EXECUTING);
				return p;
			}
		}else {
			p.setType(TypePosition.EXECUTING);
			return p;
		}
	}
	
	/** Contruir o objeto de Ordem que será gravado no banco de dados */
	private OrdemRequest buildBuyOrderRequest(OrdemRequest req, PosicaoFundo position) {
		if(req.getQtdCompra() == null && req.getQtdCompraPercent() != null) {
			if(position != null) {
				/** Caso a quantidade de compra for informada pela porcentagem */
				req.setQtdCompra(transformPercentInQtd(req, position));				
			}else {
				throw new PsiException.PsiExceptionBuilder("Não é possível realizar o calculo com o percentual informado, não foi encontrada Posição.", null)
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();	
			}
		}else {
			req.setQtdCompra(Math.round(req.getQtdCompra().floatValue() / 100 ) * 100);
		}
		req.setDataInclusao(LocalDateTime.now());
		req.setDataInclusaoFilter(LocalDate.now());
		req.setStatus(req.getStatus() == null ? Status.NEW : req.getStatus());
		return req;
	}
	
	private OrdemRequest buildOrderRequest(OrdemRequest req, PosicaoFundo positionSell, PosicaoFundo positionBuy) {
		if(req.getQtdVendaPercent() != null) {
			req.setQtdVenda(transformPercentInQtd(req, positionSell));
		}else {
			req.setQtdVenda(Math.round(req.getQtdVenda().floatValue() / 100 ) * 100);
		}
		if(req.getQtdCompraPercent() != null) {
			req.setQtdCompra(transformPercentInQtd(req, positionBuy));
		}else {
			req.setQtdCompra(Math.round(req.getQtdCompra().floatValue() / 100 ) * 100);
		}
		req.setDataInclusao(LocalDateTime.now());
		req.setDataInclusaoFilter(LocalDate.now());
		req.setStatus(req.getStatus() == null ? Status.NEW : req.getStatus());
		return req;
	}
	
	private OrdemRequest buildSellOrderRequest(OrdemRequest req, PosicaoFundo position) {
		if(!req.getTipoOperacaoOrdem().equals(TipoOrdem.ZERAR)) {
			if(req.getQtdVendaPercent() != null && req.getQtdCompra() == null) {
				req.setQtdVenda(transformPercentInQtd(req, position));
			}else {
				req.setQtdVenda(Math.round(req.getQtdVenda().floatValue() / 100 ) * 100);
			}
		}
		req.setDataInclusao(LocalDateTime.now());
		req.setDataInclusaoFilter(LocalDate.now());
		req.setStatus(req.getStatus() == null ? Status.NEW : req.getStatus());
		return req;
	}
	
	private OrdemEmsxRequest buildBuyOrderEmsxRequest(OrdemDetailResponse respOrdem) {
		OrdemEmsxRequest req = new OrdemEmsxRequest();
		
		//Campos Obrigatórios para enviar a ordem para o Terminal Bloomberg
		req.setEmsxAmount(respOrdem.getAmount());
		req.setEmsxHandInstruction("ANY");
		req.setEmsxOrderType("MKT");
		req.setEmsxSide("BUY");
		req.setEmsxTif("DAY");
		req.setEmsxTicker(getTicker(respOrdem).getBloomberg());
		req.setEmsxOrdRefId(respOrdem.getId());
		
    	return req;
	}
	
	private OrdemEmsxRequest buildSellOrderEmsxRequest(OrdemDetailResponse respOrdem) {
		OrdemEmsxRequest req = new OrdemEmsxRequest();

		//Campos Obrigatórios para enviar a ordem para o Terminal Bloomberg
		req.setEmsxAmount(respOrdem.getAmount());
		req.setEmsxHandInstruction("ANY");
		req.setEmsxOrderType("MKT");
		req.setEmsxSide("SELL");
		req.setEmsxTif("DAY");
		req.setEmsxTicker(getTicker(respOrdem).getBloomberg());
		req.setEmsxOrdRefId(respOrdem.getId());

    	return req;
	}
	
	/** Retorna o codigo do ticker reconhecido pela Bloomberg */
	private TickerResponse getTicker(OrdemDetailResponse respOrdem) {
		return this.tickerService.getById(respOrdem.getTicker());
	}
	
	/** Transforma a quantidade em porcentagem em inteiro de acordo com a Posição do Fundo */
	private Integer transformPercentInQtd(OrdemRequest req, PosicaoFundo position) {
		BigDecimal total = new BigDecimal(position.getAmount()).multiply(position.getFinancialPu());
		BigDecimal percent = req.getQtdVendaPercent().divide(new BigDecimal(100));
		BigDecimal valuePercent = total.multiply(percent).setScale(2, RoundingMode.HALF_UP);
		/** Inserir valor unitário da ação */
		BigDecimal totalQtd = valuePercent.divide(position.getFinancialPu());
		return Math.round(totalQtd.floatValue() / 100 ) * 100;
	}

}
