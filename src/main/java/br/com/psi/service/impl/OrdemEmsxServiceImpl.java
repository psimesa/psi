package br.com.psi.service.impl;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.dto.response.OrdemEmsxResponse;
import br.com.psi.entity.OrdemEmsx;
import br.com.psi.enums.ActionBloomberg;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.OrdemEmsxRepository;
import br.com.psi.service.OrdemEmsxService;
import br.com.psi.util.MethodsUtils;

@Service
public class OrdemEmsxServiceImpl implements OrdemEmsxService {

	@Autowired
	private MethodsUtils utils;
	
	@Autowired
	private OrdemEmsxRepository repository;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
    @Value("${exchange.bloomberg}")
    private String exchange;
    
    @Value("${routeKey.bloomberg.order-bloomberg}")
    private String routeOrderBloomberg;
	
	@Override
	public OrdemEmsxResponse insert(OrdemEmsxRequest req) {
		return this.save(req);
	}

	@Override
	public OrdemEmsxResponse getEmsxOrdRefId(Integer emsxOrdRefId) {
		return (OrdemEmsxResponse) this.utils.convertToDto(this.repository.findByEmsxOrdRefId(emsxOrdRefId), new OrdemEmsxResponse());
	}

	@Override
	public OrdemEmsxResponse update(OrdemEmsxRequest req) {
		try {
			req.setDataUltimaAlteracao(LocalDateTime.now());
			OrdemEmsxResponse res = this.save(req);
			this.updateOrderEmsx(res);
			return res;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private void updateOrderEmsx(OrdemEmsxResponse res) {
//		List<OrdemEmsx> entity = repository.findByIdOrdem(res.getId());
//		entity.forEach(or -> {
//			or.setEmsxAmount();
//			or = repository.save(or);
//			OrdemEmsxResponse resp = new OrdemEmsxResponse();
//			resp = (OrdemEmsxResponse) this.utils.convertToDto(or, resp);
//			resp.setAction(ActionBloomberg.ModifyOrder);
//			Gson gson = new Gson();
//			rabbitTemplate.convertAndSend(exchange, routeOrderBloomberg, gson.toJson(resp));
//		});
	}

	private OrdemEmsxResponse save(OrdemEmsxRequest req){
		try {		
			OrdemEmsx ms = (OrdemEmsx) this.utils.convertToEntity(req, new OrdemEmsx());
			OrdemEmsxResponse res = (OrdemEmsxResponse) this.utils.convertToDto(this.repository.save(ms), new OrdemEmsxResponse());
			this.utils.generateLogInfo(res);
			return res;
		}catch (Exception e) {
			this.utils.generateLogError(req);
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	@Override
	@Transactional
	public Boolean deleteByEmsxOrdRefId(Integer emsxOrdRefId) {
		try {
			OrdemEmsxResponse resp = (OrdemEmsxResponse) this.utils.convertToDto(this.repository.findByEmsxOrdRefId(emsxOrdRefId), new OrdemEmsxResponse());
			resp.setAction(ActionBloomberg.DeleteOrder);
			Gson gson = new Gson();
			rabbitTemplate.convertAndSend(exchange, routeOrderBloomberg, gson.toJson(resp));
			this.repository.deleteByEmsxOrdRefId(emsxOrdRefId);
			this.utils.generateLogInfo(resp);
			return true;
		} catch (Exception e) {
			this.utils.generateLogError("[DELETE] ORDEREMSX emsxOrdRefId:" + emsxOrdRefId);
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

}
