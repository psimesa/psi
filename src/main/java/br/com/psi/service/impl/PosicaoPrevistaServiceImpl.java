package br.com.psi.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoPrevistaRequest;
import br.com.psi.entity.PosicaoPrevista;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.PosicaoPrevistaRepository;
import br.com.psi.service.PosicaoPrevistaService;
import br.com.psi.util.MethodsUtils;

@Service
public class PosicaoPrevistaServiceImpl implements PosicaoPrevistaService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private PosicaoPrevistaRepository repository;

	@Override
	public PosicaoFundo insert(PosicaoPrevistaRequest req) {
		req.setDataInclusao(LocalDateTime.now());
		return this.save(req);
	}


	@Override
	public PosicaoFundo getByTickerAndBookAndFundo(Integer ticker, Integer book, Integer fundo) {
		try {
			List<PosicaoPrevista> list = this.repository.findByProductAndBookAndFundo(ticker, book, fundo);
			return list.isEmpty() ? null : this.buildPosition(list.get(list.size() - 1));
		}catch (Exception e) {
			 throw new PsiException.PsiExceptionBuilder("Não foi possível encontrar Posição para o Fundo.", e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private PosicaoFundo save(PosicaoPrevistaRequest req){
		return this.buildPosition(this.repository.save((PosicaoPrevista) this.utils.convertToEntity(req, new PosicaoPrevista())));
	}

	private PosicaoFundo buildPosition(PosicaoPrevista entity) {
		PosicaoFundo res = new PosicaoFundo();
		res.setAmount(entity.getAmount());
		res.setBook(entity.getBook());
		res.setFinancialPu(entity.getFinancialPu());
		res.setFund(entity.getFundo());
		res.setProduct(entity.getProduct());
		return res;
	}


	@Override
	@Transactional
	public Boolean deleteByIdOrdemDetail(Integer idOrdemDetail) {
		try {
			this.repository.deleteByIdOrdemDetail(idOrdemDetail);
			this.utils.generateLogInfo(idOrdemDetail);
			return true;
		} catch (Exception e) {
			this.utils.generateLogError(idOrdemDetail);
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

}
