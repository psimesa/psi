package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.TickerRequest;
import br.com.psi.dto.response.TickerResponse;
import br.com.psi.entity.Ticker;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.TickerRepository;
import br.com.psi.service.TickerService;
import br.com.psi.util.MethodsUtils;

@Service
public class TickerServiceImpl implements TickerService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private TickerRepository repository;

	@Override
	public TickerResponse insert(TickerRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public List<TickerResponse> getAll() {
		List<TickerResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			TickerResponse res = new TickerResponse();
			list.add((TickerResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public TickerResponse getById(Integer id) {
		try {
			TickerResponse res = new TickerResponse();
			return (TickerResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public TickerResponse update(TickerRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}
	
	private TickerResponse save(TickerRequest req){
		TickerResponse res = new TickerResponse();
		Ticker ms = new Ticker();
		ms = (Ticker) this.utils.convertToEntity(req, ms);
		return (TickerResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}

	@Override
	public TickerResponse getByTicker(String ticker) {
		TickerResponse res = new TickerResponse();
		return (TickerResponse) this.utils.convertToDto(this.repository.findByName(ticker), res);
	}
	
}
