package br.com.psi.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.GrupoEnquadramentoRequest;
import br.com.psi.dto.response.GrupoEnquadramentoResponse;
import br.com.psi.entity.GrupoEnquadramento;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.GrupoEnquadramentoRepository;
import br.com.psi.service.GrupoEnquadramentoService;
import br.com.psi.util.MethodsUtils;

@Service
public class GrupoEnquadramentoServiceImpl implements GrupoEnquadramentoService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private GrupoEnquadramentoRepository repository;

	@Override
	public GrupoEnquadramentoResponse insert(GrupoEnquadramentoRequest req) {
		try {
			req.setDataInclusao(LocalDateTime.now());
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public List<GrupoEnquadramentoResponse> getAll() {
		List<GrupoEnquadramentoResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			GrupoEnquadramentoResponse res = new GrupoEnquadramentoResponse();
			list.add((GrupoEnquadramentoResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public GrupoEnquadramentoResponse getById(Integer id) {
		try {
			GrupoEnquadramentoResponse res = new GrupoEnquadramentoResponse();
			return (GrupoEnquadramentoResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public GrupoEnquadramentoResponse update(GrupoEnquadramentoRequest req) {
		try {
			req.setDataUltimaAlteracao(LocalDateTime.now());
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private GrupoEnquadramentoResponse save(GrupoEnquadramentoRequest req){
		GrupoEnquadramentoResponse res = new GrupoEnquadramentoResponse();
		GrupoEnquadramento ms = new GrupoEnquadramento();
		ms = (GrupoEnquadramento) this.utils.convertToEntity(req, ms);
		try {		
			res = (GrupoEnquadramentoResponse) this.utils.convertToDto(this.repository.save(ms), res);
			this.utils.generateLogInfo(res);
			return res;
		}catch (Exception e) {
			this.utils.generateLogError(req);
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
}
