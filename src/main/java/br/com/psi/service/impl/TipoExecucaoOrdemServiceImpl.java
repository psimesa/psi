package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.TipoExecucaoOrdemRequest;
import br.com.psi.dto.response.TipoExecucaoOrdemResponse;
import br.com.psi.entity.TipoExecucaoOrdem;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.TipoExecucaoOrdemRepository;
import br.com.psi.service.TipoExecucaoOrdemService;
import br.com.psi.util.MethodsUtils;

@Service
public class TipoExecucaoOrdemServiceImpl implements TipoExecucaoOrdemService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private TipoExecucaoOrdemRepository repository;

	@Override
	public TipoExecucaoOrdemResponse insert(TipoExecucaoOrdemRequest req) {
		TipoExecucaoOrdemResponse res = new TipoExecucaoOrdemResponse();
		TipoExecucaoOrdem ms = new TipoExecucaoOrdem();
		ms = (TipoExecucaoOrdem) this.utils.convertToEntity(req, ms);
		return (TipoExecucaoOrdemResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}

	@Override
	public List<TipoExecucaoOrdemResponse> getAll() {
		List<TipoExecucaoOrdemResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			TipoExecucaoOrdemResponse res = new TipoExecucaoOrdemResponse();
			list.add((TipoExecucaoOrdemResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public TipoExecucaoOrdemResponse getById(Integer id) {
		try {
			TipoExecucaoOrdemResponse res = new TipoExecucaoOrdemResponse();
			return (TipoExecucaoOrdemResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public TipoExecucaoOrdemResponse update(TipoExecucaoOrdemRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private TipoExecucaoOrdemResponse save(TipoExecucaoOrdemRequest req){
		TipoExecucaoOrdemResponse res = new TipoExecucaoOrdemResponse();
		TipoExecucaoOrdem ms = new TipoExecucaoOrdem();
		ms = (TipoExecucaoOrdem) this.utils.convertToEntity(req, ms);
		return (TipoExecucaoOrdemResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}
	
}
