package br.com.psi.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoAberturaRequest;
import br.com.psi.dto.response.PosicaoAberturaResponse;
import br.com.psi.entity.PosicaoAbertura;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.PosicaoAberturaRepository;
import br.com.psi.service.PosicaoAberturaService;
import br.com.psi.util.MethodsUtils;

@Service
public class PosicaoAberturaServiceImpl implements PosicaoAberturaService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private PosicaoAberturaRepository repository;

	@Override
	public PosicaoFundo insert(PosicaoAberturaRequest req) {
		req.setDataInclusao(LocalDateTime.now());
		return this.save(req);
	}


	@Override
	public PosicaoFundo getByTickerAndBookAndFundo(Integer ticker, Integer book, Integer fundo) {
		try {
			PosicaoAbertura res = this.repository.findByProductAndBookAndFundo(ticker, book, fundo);
			return res == null ? null : this.buildPosition(res);
		}catch (Exception e) {
			 throw new PsiException.PsiExceptionBuilder("Não foi possível encontrar Posição de Abertura para o Fundo.", e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private PosicaoFundo save(PosicaoAberturaRequest req){
		PosicaoAbertura entity = new PosicaoAbertura();
		entity = this.repository.save((PosicaoAbertura) this.utils.convertToEntity(req, entity));
		return this.buildPosition(entity);
	}


	private PosicaoFundo buildPosition(PosicaoAbertura entity) {
		PosicaoFundo res = new PosicaoFundo();
		res.setAmount(entity.getAmount());
		res.setBook(entity.getBook());
		res.setFinancialPu(entity.getFinancialPu());
		res.setFund(entity.getFundo());
		res.setProduct(entity.getProduct());
		return res;
	}

	@Override
	public List<PosicaoAberturaResponse> getByFundoAndBook(Integer fundo, Integer book) {
		List<PosicaoAberturaResponse> res = new ArrayList<>();
		List<PosicaoAbertura> po = this.repository.findByFundoAndBook(fundo, book);
		return null;
	}

}
