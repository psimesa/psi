package br.com.psi.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.psi.dto.request.OrdemDetailRequest;
import br.com.psi.dto.response.OrdemDetailResponse;
import br.com.psi.entity.OrdemDetail;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.OrdemDetailRepository;
import br.com.psi.service.OrdemDetailService;
import br.com.psi.service.OrdemEmsxService;
import br.com.psi.service.PosicaoExecutadaService;
import br.com.psi.service.PosicaoPrevistaService;
import br.com.psi.util.MethodsUtils;

@Service
public class OrdemDetailServiceImpl implements OrdemDetailService {

	@Autowired
	private MethodsUtils utils;
	
	@Autowired
	private OrdemDetailRepository repository;
	
	@Autowired
	private OrdemEmsxService emsxService;
	
	@Autowired
	private PosicaoPrevistaService posicaoPrevistaService;
	
	@Autowired
	private PosicaoExecutadaService posicaoExecutadaService;
	
	@Override
	public OrdemDetailResponse insert(OrdemDetailRequest req) {
		return this.save(req);
	}

	@Override
	public OrdemDetailResponse update(OrdemDetailRequest req) {
		try {
			req.setDataUltimaAlteracao(LocalDateTime.now());
			OrdemDetailResponse res = this.save(req);
			this.updateOrderEmsx(res);
			return res;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private void updateOrderEmsx(OrdemDetailResponse res) {
//		List<OrdemEmsx> entity = repository.findByIdOrdem(res.getId());
//		entity.forEach(or -> {
//			or.setEmsxAmount();
//			or = repository.save(or);
//			OrdemEmsxResponse resp = new OrdemEmsxResponse();
//			resp = (OrdemEmsxResponse) this.utils.convertToDto(or, resp);
//			resp.setAction(ActionBloomberg.ModifyOrder);
//			Gson gson = new Gson();
//			rabbitTemplate.convertAndSend(exchange, routeOrderBloomberg, gson.toJson(resp));
//		});
	}

	private OrdemDetailResponse save(OrdemDetailRequest req){
		try {		
			OrdemDetail ms = (OrdemDetail) this.utils.convertToEntity(req, new OrdemDetail());
			OrdemDetailResponse res = (OrdemDetailResponse) this.utils.convertToDto(this.repository.save(ms), new OrdemDetailResponse());
			this.utils.generateLogInfo(res);
			return res;
		}catch (Exception e) {
			this.utils.generateLogError(req);
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public OrdemDetailResponse getById(Integer id) {
		OrdemDetailResponse res = (OrdemDetailResponse) this.utils.convertToDto(repository.findById(id).get(), new OrdemDetailResponse());
		return res;
	}

	@Override
	public Boolean delete(Integer id) {
		OrdemDetail res = repository.findById(id).get();
		this.repository.deleteById(res.getId());
		return true;
	}

	@Override
	@Transactional
	public Boolean deleteByIdOrdem(Integer idOrdem) {
		try {
			List<OrdemDetail> res = repository.findByIdOrdem(idOrdem);
			res.forEach(o -> {
				this.emsxService.deleteByEmsxOrdRefId(o.getId());
				this.posicaoExecutadaService.deleteByIdOrdemDetail(o.getId());
				this.posicaoPrevistaService.deleteByIdOrdemDetail(o.getId());
				this.delete(o.getId());
			});
			this.utils.generateLogInfo(res);
			return true;
		} catch (Exception e) {
			this.utils.generateLogError(idOrdem);
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}
	
}
