package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.TipoOperacaoOrdemRequest;
import br.com.psi.dto.response.TipoOperacaoOrdemResponse;
import br.com.psi.entity.TipoOperacaoOrdem;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.TipoOperacaoOrdemRepository;
import br.com.psi.service.TipoOperacaoOrdemService;
import br.com.psi.util.MethodsUtils;

@Service
public class TipoOperacaoOrdemServiceImpl implements TipoOperacaoOrdemService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private TipoOperacaoOrdemRepository repository;

	@Override
	public TipoOperacaoOrdemResponse insert(TipoOperacaoOrdemRequest req) {
		TipoOperacaoOrdemResponse res = new TipoOperacaoOrdemResponse();
		TipoOperacaoOrdem ms = new TipoOperacaoOrdem();
		ms = (TipoOperacaoOrdem) this.utils.convertToEntity(req, ms);
		return (TipoOperacaoOrdemResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}

	@Override
	public List<TipoOperacaoOrdemResponse> getAll() {
		List<TipoOperacaoOrdemResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			TipoOperacaoOrdemResponse res = new TipoOperacaoOrdemResponse();
			list.add((TipoOperacaoOrdemResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public TipoOperacaoOrdemResponse getById(Integer id) {
		try {
			TipoOperacaoOrdemResponse res = new TipoOperacaoOrdemResponse();
			return (TipoOperacaoOrdemResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public TipoOperacaoOrdemResponse update(TipoOperacaoOrdemRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
	      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
	          .httpStatus(HttpStatus.BAD_REQUEST)
	          .build();
		}
	}
	
	private TipoOperacaoOrdemResponse save(TipoOperacaoOrdemRequest req){
		TipoOperacaoOrdemResponse res = new TipoOperacaoOrdemResponse();
		TipoOperacaoOrdem ms = new TipoOperacaoOrdem();
		ms = (TipoOperacaoOrdem) this.utils.convertToEntity(req, ms);
		return (TipoOperacaoOrdemResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}
	
}
