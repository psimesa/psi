package br.com.psi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.psi.dto.request.RebalanceRequest;
import br.com.psi.dto.response.RebalanceResponse;
import br.com.psi.entity.Rebalance;
import br.com.psi.exception.PsiException;
import br.com.psi.repository.RebalanceRepository;
import br.com.psi.service.CotaFundoInvestimentoService;
import br.com.psi.service.FundoService;
import br.com.psi.service.PosicaoAberturaService;
import br.com.psi.service.PosicaoExecutadaService;
import br.com.psi.service.PosicaoPrevistaService;
import br.com.psi.service.RebalanceService;
import br.com.psi.util.MethodsUtils;

@Service
public class RebalanceServiceImpl implements RebalanceService {

	@Autowired
	private MethodsUtils utils;

	@Autowired
	private RebalanceRepository repository;
	
	@Autowired
	private FundoService fundoService;
	
	@Autowired
	private CotaFundoInvestimentoService bookService;
	
	@Autowired
	private PosicaoAberturaService posicaoAberturaService;
	
	@Autowired
	private PosicaoExecutadaService posicaoExecService;
	
	@Autowired
	private PosicaoPrevistaService posicaoPrevistaService;

	@Override
	public RebalanceResponse insert(RebalanceRequest req) {
		try {
//			this.posicaoAberturaService.getByFundoAndBook(req.getFundo(), )
			
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public List<RebalanceResponse> getAll() {
		List<RebalanceResponse> list = new ArrayList<>();
		this.repository.findAll().forEach(c -> {
			RebalanceResponse res = new RebalanceResponse();
			list.add((RebalanceResponse) this.utils.convertToDto(c, res));
		});
		return list;
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			this.repository.deleteById(id);
			return true;
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public RebalanceResponse getById(Integer id) {
		try {
			RebalanceResponse res = new RebalanceResponse();
			return (RebalanceResponse) this.utils.convertToDto(this.repository.getOne(id), res);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}

	@Override
	public RebalanceResponse update(RebalanceRequest req) {
		try {
			return this.save(req);
		}catch (Exception e) {
		      System.out.println(e.getMessage());
		      throw new PsiException.PsiExceptionBuilder(e.getMessage(), e.getCause())
		          .httpStatus(HttpStatus.BAD_REQUEST)
		          .build();
		}
	}
	
	private RebalanceResponse save(RebalanceRequest req){
		RebalanceResponse res = new RebalanceResponse();
		Rebalance ms = new Rebalance();
		ms = (Rebalance) this.utils.convertToEntity(req, ms);
		return (RebalanceResponse) this.utils.convertToDto(this.repository.save(ms), res);
	}
	
}
