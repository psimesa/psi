package br.com.psi.service;

import java.time.LocalDate;
import java.util.List;

import br.com.psi.bo.response.OrdemBO;
import br.com.psi.dto.request.OrdemRequest;
import br.com.psi.dto.response.OrdemResponse;
import br.com.psi.enums.Status;

public interface OrdemService {

	public OrdemResponse insert(OrdemRequest req);

	public List<OrdemResponse> getAll();
	
	public OrdemResponse getById(Integer id);

	public OrdemResponse update(OrdemRequest req);
	
	public Boolean delete(Integer id);

	public List<OrdemBO> getFiltered(List<Status> status, LocalDate data);
	
}
