package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoAberturaRequest;
import br.com.psi.dto.response.PosicaoAberturaResponse;

public interface PosicaoAberturaService {

	public PosicaoFundo insert(PosicaoAberturaRequest req);

	public PosicaoFundo getByTickerAndBookAndFundo(Integer ticker, Integer book, Integer fundo);

	public List<PosicaoAberturaResponse> getByFundoAndBook(Integer fundo, Integer book);

}
