package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.RebalanceRequest;
import br.com.psi.dto.response.RebalanceResponse;

public interface RebalanceService {

	public RebalanceResponse insert(RebalanceRequest req);

	public List<RebalanceResponse> getAll();
	
	public Boolean delete(Integer id);
	
	public RebalanceResponse getById(Integer id);

	public RebalanceResponse update(RebalanceRequest req);
	
}
