package br.com.psi.service.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.dto.request.OrdemRequest;
import br.com.psi.dto.response.OrdemEmsxResponse;
import br.com.psi.dto.response.OrdemResponse;
import br.com.psi.dto.response.TickerResponse;
import br.com.psi.enums.ActionBloomberg;
import br.com.psi.enums.Status;
import br.com.psi.service.OrdemEmsxService;
import br.com.psi.service.OrdemService;
import br.com.psi.service.PosicaoAberturaService;
import br.com.psi.service.PosicaoExecutadaService;
import br.com.psi.service.PosicaoPrevistaService;
import br.com.psi.service.TickerService;

@Service
public class OrderTypeService {
	
	@Autowired
	private PosicaoExecutadaService posicaoExecutadaService;
	
	@Autowired
	private PosicaoPrevistaService posicaoPrevistaService;
	
	@Autowired
	private PosicaoAberturaService posicaoAberturaService;
	
	@Autowired
	private OrdemService ordemService;
	
	@Autowired
	private OrdemEmsxService ordemEmsxService;
	
	@Autowired
	private TickerService tickerService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
    @Value("${exchange.bloomberg}")
    private String exchange;
    
    @Value("${routeKey.bloomberg.order-bloomberg}")
    private String routeOrderBloomberg;
    
    public OrdemResponse init(OrdemRequest req) {
    	switch (req.getTipoOperacaoOrdem()) {
		case ZERAR:
			return this.reset(req);
		case EQUAL:
			return null;
		case COMPRA:
			return null;
		case VENDA:
			return null;
		default:
			return null;
		}
    }

	/** Tipo da ordem (Zerar), zera o ticker venda, com o valor retornado da venda comprar o ticker compra */
    private OrdemResponse reset(OrdemRequest req) {
		PosicaoFundo position = this.getFundPosition(req);
		OrdemResponse respOrdem = new OrdemResponse();
		if(req.getId() == null) {
			respOrdem = null;
			this.ordemEmsxService.insert(this.sendOrdemEmsxRequest(respOrdem, ActionBloomberg.CreateOrder));			
		}else {
			respOrdem = this.ordemService.update(this.buildOrdemRequest(req, position));
			this.ordemEmsxService.insert(this.sendOrdemEmsxRequest(respOrdem, ActionBloomberg.ModifyOrder));
		}
		return respOrdem;
	}

	/** Tipo da ordem (Equal), iguala a carteira ao índice Ibovespa */
	public void equal(OrdemRequest req) {
		
	}
	
	/** Tipo da ordem (Vender) */
	public void sell(OrdemRequest req) {
		
	}
	
	/** Tipo da ordem (Comprar) */
	public void buy(OrdemRequest req) {
		
	}
	
	/** Tipo da ordem (L&S) */
	public void longAndShort(OrdemRequest req) {
		
	}
	
	public void resetSellingBuyingTicker(OrdemRequest req) {
		
	}
	
	public void neutralizeBuySelling(OrdemRequest req) {
		
	}
	
	public void neutralizeSellBuying(OrdemRequest req) {
		
	}
	
	private OrdemRequest buildOrdemRequest(OrdemRequest req, PosicaoFundo position) {
		if(req.getQtdCompraPercent() == null && req.getQtdCompra() == null) {
			req.setQtdVenda(transformPercentInQtd(req, position));
		}else {
			req.setQtdCompra(transformPercentInQtd(req, position));
		}
		req.setDataInclusao(LocalDateTime.now());
		req.setDataInclusaoFilter(LocalDate.now());
		req.setStatus(req.getStatus() == null ? Status.NEW : req.getStatus());
		return req;
	}
	
	private OrdemEmsxRequest sendOrdemEmsxRequest(OrdemResponse respOrdem, ActionBloomberg action) {
		OrdemEmsxRequest req = new OrdemEmsxRequest();
		OrdemEmsxResponse resp = new OrdemEmsxResponse();
    	if(action.equals(ActionBloomberg.CreateOrder)) {
    		//Campos Obrigatórios para enviar a ordem para o Terminal Bloomberg
    		req.setEmsxAmount(respOrdem.getQtdCompra() == null ? respOrdem.getQtdVenda() : respOrdem.getQtdCompra());
    		req.setEmsxHandInstruction("ANY");
    		req.setEmsxOrderType("MKT");
    		req.setEmsxSide(respOrdem.getQtdCompra() == null ? "SELL" : "BUY");
    		req.setEmsxTif("DAY");
    		req.setEmsxTicker(getTickerBloomberg(respOrdem));
    		req.setEmsxOrdRefId(respOrdem.getId());
    		resp = this.ordemEmsxService.insert(req);    		
    	}else {
    		resp = this.ordemEmsxService.getEmsxOrdRefId(respOrdem.getId());
    		resp.setEmsxAmount(resp.getEmsxSide().equals("SELL") ? respOrdem.getQtdVenda() : respOrdem.getQtdCompra());
    	}
		resp.setAction(action);
		Gson gson = new Gson();
		rabbitTemplate.convertAndSend(exchange, routeOrderBloomberg, gson.toJson(resp));
    	return req;
	}
	
	private String getTickerBloomberg(OrdemResponse respOrdem) {
		TickerResponse ticker = tickerService.getById(respOrdem.getTickerCompra() == null ? respOrdem.getTickerVenda() : respOrdem.getTickerCompra());
		return ticker.getBloomberg();
	}

	private PosicaoFundo getFundPosition(OrdemRequest req) {
		PosicaoFundo p = new PosicaoFundo();
		p = this.posicaoPrevistaService.getByTickerAndBookAndFundo(req.getTickerCompra() == null ? req.getTickerVenda() : req.getTickerCompra(), req.getCotaFundoInvestimento(), req.getFundo());
		if(p == null) {
			p = this.posicaoAberturaService.getByTickerAndBookAndFundo(req.getTickerCompra() == null ? req.getTickerVenda() : req.getTickerCompra(), req.getCotaFundoInvestimento(), req.getFundo());
		}
		return p;
	}
	
	private Integer transformPercentInQtd(OrdemRequest req, PosicaoFundo position) {
		BigDecimal total = new BigDecimal(position.getAmount()).multiply(position.getFinancialPu());
		BigDecimal valuePercent = total.multiply(req.getQtdVendaPercent());
		/** Inserir valor unitário da ação */
		BigDecimal totalQtd = valuePercent.divide(position.getFinancialPu()).setScale(2, RoundingMode.HALF_UP);
		return totalQtd.intValue();
	}
	
}
