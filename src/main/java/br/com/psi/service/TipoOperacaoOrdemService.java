package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.TipoOperacaoOrdemRequest;
import br.com.psi.dto.response.TipoOperacaoOrdemResponse;

public interface TipoOperacaoOrdemService {

	public TipoOperacaoOrdemResponse insert(TipoOperacaoOrdemRequest req);

	public List<TipoOperacaoOrdemResponse> getAll();
	
	public TipoOperacaoOrdemResponse getById(Integer id);

	public TipoOperacaoOrdemResponse update(TipoOperacaoOrdemRequest req);
	
	public Boolean delete(Integer id);
	
}
