package br.com.psi.service;

import br.com.psi.dto.request.IndiceBovespaRequest;
import br.com.psi.dto.response.IndiceBovespaResponse;

public interface IndiceBovespaService {

	public IndiceBovespaResponse insert(IndiceBovespaRequest req);

	public IndiceBovespaResponse getByTicker(String ticker);
	
}
