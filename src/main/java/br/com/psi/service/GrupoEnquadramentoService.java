package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.GrupoEnquadramentoRequest;
import br.com.psi.dto.response.GrupoEnquadramentoResponse;

public interface GrupoEnquadramentoService {

	public List<GrupoEnquadramentoResponse> getAll();

	public GrupoEnquadramentoResponse insert(GrupoEnquadramentoRequest req);
	
	public GrupoEnquadramentoResponse getById(Integer id);

	public GrupoEnquadramentoResponse update(GrupoEnquadramentoRequest req);
	
	public Boolean delete(Integer id);
	
}
