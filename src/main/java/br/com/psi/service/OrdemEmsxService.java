package br.com.psi.service;

import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.dto.response.OrdemEmsxResponse;

public interface OrdemEmsxService {

	public OrdemEmsxResponse insert(OrdemEmsxRequest req);

	public OrdemEmsxResponse getEmsxOrdRefId(Integer id);
	
	public Boolean deleteByEmsxOrdRefId(Integer id);

	public OrdemEmsxResponse update(OrdemEmsxRequest req);
	
}
