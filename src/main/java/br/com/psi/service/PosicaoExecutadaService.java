package br.com.psi.service;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoExecutadaRequest;

public interface PosicaoExecutadaService {

	public PosicaoFundo insert(PosicaoExecutadaRequest req);

	public PosicaoFundo getByTickerAndBookAndFundo(String ticker, String book, String fundo);
	
	public Boolean deleteByIdOrdemDetail(Integer idOrdemDetail);
	
}
