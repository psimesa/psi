package br.com.psi.service;

import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;

import br.com.psi.dto.request.FundoRequest;
import br.com.psi.dto.response.FundoResponse;

public interface FundoService {

	public List<FundoResponse> getAll();

	public FundoResponse insert(FundoRequest req);

	public Boolean upload(MultipartFile file) throws IOException, InvalidFormatException;
	
	public FundoResponse getById(Integer id);
	
	public FundoResponse getByFundo(String fundo);

	public FundoResponse update(FundoRequest req);
	
	public Boolean delete(Integer id);
	
}
