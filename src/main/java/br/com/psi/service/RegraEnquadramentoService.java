package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.RegraEnquadramentoRequest;
import br.com.psi.dto.response.RegraEnquadramentoResponse;

public interface RegraEnquadramentoService {

	public List<RegraEnquadramentoResponse> getAll();

	public RegraEnquadramentoResponse insert(RegraEnquadramentoRequest req);
	
	public RegraEnquadramentoResponse getById(Integer id);

	public RegraEnquadramentoResponse update(RegraEnquadramentoRequest req);
	
	public Boolean delete(Integer id);
	
}
