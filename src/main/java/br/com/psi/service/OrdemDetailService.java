package br.com.psi.service;

import br.com.psi.dto.request.OrdemDetailRequest;
import br.com.psi.dto.response.OrdemDetailResponse;

public interface OrdemDetailService {

	public OrdemDetailResponse insert(OrdemDetailRequest req);

	public OrdemDetailResponse getById(Integer id);

	public OrdemDetailResponse update(OrdemDetailRequest req);
	
	public Boolean delete(Integer id);
	
	public Boolean deleteByIdOrdem(Integer idOrdem);

}
