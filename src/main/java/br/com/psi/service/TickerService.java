package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.TickerRequest;
import br.com.psi.dto.response.TickerResponse;

public interface TickerService {

	public TickerResponse insert(TickerRequest req);

	public List<TickerResponse> getAll();
	
	public TickerResponse getById(Integer id);
	
	public TickerResponse getByTicker(String ticker);

	public TickerResponse update(TickerRequest req);
	
	public Boolean delete(Integer id);
	
}
