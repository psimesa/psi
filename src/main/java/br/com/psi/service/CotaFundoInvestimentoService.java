package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.CotaFundoInvestimentoRequest;
import br.com.psi.dto.response.CotaFundoInvestimentoResponse;

public interface CotaFundoInvestimentoService {

	public CotaFundoInvestimentoResponse insert(CotaFundoInvestimentoRequest req);

	public List<CotaFundoInvestimentoResponse> getAll();
	
	public CotaFundoInvestimentoResponse getById(Integer id);

	public CotaFundoInvestimentoResponse update(CotaFundoInvestimentoRequest req);
	
	public Boolean delete(Integer id);

	public List<CotaFundoInvestimentoResponse> getByIdFundo(Integer idFundo);
	
	public CotaFundoInvestimentoResponse getByName(String name);
	
}
