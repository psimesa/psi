package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.ContaMasterRequest;
import br.com.psi.dto.response.ContaMasterResponse;

public interface ContaMasterService {

	public ContaMasterResponse insert(ContaMasterRequest req);

	public List<ContaMasterResponse> getAll();
	
	public Boolean delete(Integer id);
	
	public ContaMasterResponse getById(Integer id);

	public ContaMasterResponse update(ContaMasterRequest req);
	
}
