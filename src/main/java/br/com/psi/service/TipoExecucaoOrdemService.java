package br.com.psi.service;

import java.util.List;

import br.com.psi.dto.request.TipoExecucaoOrdemRequest;
import br.com.psi.dto.response.TipoExecucaoOrdemResponse;

public interface TipoExecucaoOrdemService {

	public TipoExecucaoOrdemResponse insert(TipoExecucaoOrdemRequest req);

	public List<TipoExecucaoOrdemResponse> getAll();
	
	public TipoExecucaoOrdemResponse getById(Integer id);

	public TipoExecucaoOrdemResponse update(TipoExecucaoOrdemRequest req);
	
	public Boolean delete(Integer id);
	
}
