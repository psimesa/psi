package br.com.psi.service;

import br.com.psi.dto.PosicaoFundo;
import br.com.psi.dto.request.PosicaoPrevistaRequest;

public interface PosicaoPrevistaService {

	public PosicaoFundo insert(PosicaoPrevistaRequest req);

	public PosicaoFundo getByTickerAndBookAndFundo(Integer ticker, Integer book, Integer fundo);
	
	public Boolean deleteByIdOrdemDetail(Integer idOrdemDetail);
	
}
