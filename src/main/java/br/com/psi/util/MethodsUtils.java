package br.com.psi.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import org.modelmapper.ModelMapper;
import br.com.psi.dto.util.AbstractRequest;
import br.com.psi.dto.util.AbstractResponse;
import br.com.psi.entity.util.BaseEntity;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MethodsUtils {
	
	/** O mapeador do modelo. */
	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Converte bandeira para dto.
	 * 
	 * @param flag a bandeira
	 * @return a bandeira(response)
	 */

	public AbstractResponse convertToDto(BaseEntity entity, AbstractResponse response) {
		return modelMapper.map(entity, response.getClass());
	}

	/**
	 * Converte para entidade.
	 * 
	 * @param flagRequest a bandeira(request)
	 * @return a bandeira
	 */
	public BaseEntity convertToEntity(AbstractRequest request, BaseEntity entity) {
		return modelMapper.map(request, entity.getClass());
	}
	
	public void generateLogInfo(Object req) {
		Gson gson = new Gson();
		log.info(gson.toJson(req));
	}
	
	public void generateLogError(Object req) {
		Gson gson = new Gson();
		log.error(gson.toJson(req));
	}
	
}
