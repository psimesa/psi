package br.com.psi.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;
import com.google.gson.Gson;

import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.dto.response.OrdemEmsxResponse;

@Service
public class BloombergOrder {

	private String d_service;
    private String d_host;
    private int d_port;
    
    private CorrelationID requestID;
    
	/* Método responsável por enviar ordem Bloomberg.
	 * Payload ordem.
	*/
	@RabbitListener(queues = {"${queue.bloomberg.send-order}"})
    public void sendToBloomberg(@Payload String ordem) throws java.lang.Exception
    {
        BloombergOrder example = new BloombergOrder();
        example.run(this.saveOrderEMSX(ordem));
    }
    
    private OrdemEmsxRequest saveOrderEMSX(String ordem) {
    	Gson gson = new Gson();
    	OrdemEmsxResponse or = gson.fromJson(ordem, OrdemEmsxResponse.class);
    	return this.buildOrderEmsx(or);
	}

	private OrdemEmsxRequest buildOrderEmsx(OrdemEmsxResponse or) {
		OrdemEmsxRequest ordemEMSX = new OrdemEmsxRequest();
    	
    	//Campos Obrigatórios para enviar a ordem para o Terminal Bloomberg
    	ordemEMSX.setEmsxAmount(or.getEmsxAmount());
    	ordemEMSX.setEmsxHandInstruction(or.getEmsxHandInstruction());
    	ordemEMSX.setEmsxOrderType(or.getEmsxOrderType());
    	ordemEMSX.setEmsxSide(or.getEmsxSide());
    	ordemEMSX.setEmsxTicker(or.getEmsxTicker());
    	ordemEMSX.setEmsxTif(or.getEmsxTif());
    	ordemEMSX.setAction(or.getAction());
    	
    	//Campos complementares
    	ordemEMSX.setApiSeqNum(or.getApiSeqNum() == null ? null : or.getApiSeqNum());
    	ordemEMSX.setEmsxAccount(or.getEmsxAccount() == null ? null : or.getEmsxAccount());
    	ordemEMSX.setEmsxArrivalPrice(or.getEmsxArrivalPrice() == null ? null : or.getEmsxArrivalPrice());
    	ordemEMSX.setEmsxAssetClass(or.getEmsxAssetClass() == null ? null : or.getEmsxAssetClass());
    	ordemEMSX.setEmsxAssignedTrader(or.getEmsxAssignedTrader() == null ? null : or.getEmsxAssignedTrader());
    	ordemEMSX.setEmsxAvgPrice(or.getEmsxAvgPrice() == null ? null : or.getEmsxAvgPrice());
    	ordemEMSX.setEmsxBasketName(or.getEmsxBasketName() == null ? null : or.getEmsxBasketName());
    	ordemEMSX.setEmsxBlockId(or.getEmsxBlockId() == null ? null : or.getEmsxBlockId());
    	ordemEMSX.setEmsxBroker(or.getEmsxBroker() == null ? null : or.getEmsxBroker());
    	ordemEMSX.setEmsxBrokerComm(or.getEmsxBrokerComm() == null ? null : or.getEmsxBrokerComm());
    	ordemEMSX.setEmsxBseAvgPrice(or.getEmsxBseAvgPrice() == null ? null : or.getEmsxBseAvgPrice());
    	ordemEMSX.setEmsxBseFilled(or.getEmsxBseFilled() == null ? null : or.getEmsxBseFilled());
    	ordemEMSX.setEmsxBuysideLei(or.getEmsxBuysideLei() == null ? null : or.getEmsxBuysideLei());
    	ordemEMSX.setEmsxClientIdentification(or.getEmsxClientIdentification() == null ? null : or.getEmsxClientIdentification());
    	ordemEMSX.setEmsxCfdFlag(or.getEmsxCfdFlag() == null ? null : or.getEmsxCfdFlag());
    	ordemEMSX.setEmsxCommDiffFlag(or.getEmsxCommDiffFlag() == null ? null : or.getEmsxCommDiffFlag());
    	ordemEMSX.setEmsxCommRate(or.getEmsxCommRate() == null ? null : or.getEmsxCommRate());
    	ordemEMSX.setEmsxCurrencyPair(or.getEmsxCurrencyPair() == null ? null : or.getEmsxCurrencyPair());
    	ordemEMSX.setEmsxDate(or.getEmsxDate() == null ? null : or.getEmsxDate());
    	ordemEMSX.setEmsxDayAvgPrice(or.getEmsxDayAvgPrice() == null ? null : or.getEmsxDayAvgPrice());
    	ordemEMSX.setEmsxDayFill(or.getEmsxDayFill() == null ? null : or.getEmsxDayFill());
    	ordemEMSX.setEmsxDirBrokerFlag(or.getEmsxDirBrokerFlag() == null ? null : or.getEmsxDirBrokerFlag());
    	ordemEMSX.setEmsxExchange(or.getEmsxExchange() == null ? null : or.getEmsxExchange());
    	ordemEMSX.setEmsxExchangeDestination(or.getEmsxExchangeDestination() == null ? null : or.getEmsxExchangeDestination());
    	ordemEMSX.setEmsxExecInstruction(or.getEmsxExecInstruction() == null ? null : or.getEmsxExecInstruction());
    	ordemEMSX.setEmsxFillId(or.getEmsxFillId() == null ? null : or.getEmsxFillId());
    	ordemEMSX.setEmsxFilled(or.getEmsxFilled() == null ? null : or.getEmsxFilled());
    	ordemEMSX.setEmsxGpi(or.getEmsxGpi() == null ? null : or.getEmsxGpi());
    	ordemEMSX.setEmsxGtdDate(or.getEmsxGtdDate() == null ? null : or.getEmsxGtdDate());
    	ordemEMSX.setEmsxIdleAmount(or.getEmsxIdleAmount() == null ? null : or.getEmsxIdleAmount());
    	ordemEMSX.setEmsxInvestorId(or.getEmsxInvestorId() == null ? null : or.getEmsxInvestorId());
    	ordemEMSX.setEmsxIsin(or.getEmsxIsin() == null ? null : or.getEmsxIsin());
    	ordemEMSX.setEmsxLimitPrice(or.getEmsxLimitPrice() == null ? null : or.getEmsxLimitPrice());
    	ordemEMSX.setEmsxMifidIiInstruction(or.getEmsxMifidIiInstruction() == null ? null : or.getEmsxMifidIiInstruction());
    	ordemEMSX.setEmsxModPendStatus(or.getEmsxModPendStatus() == null ? null : or.getEmsxModPendStatus());
    	ordemEMSX.setEmsxNotes(or.getEmsxNotes() == null ? null : or.getEmsxNotes());
    	ordemEMSX.setEmsxNseAvgPrice(or.getEmsxNseAvgPrice() == null ? null : or.getEmsxNseAvgPrice());
    	ordemEMSX.setEmsxNseFilled(or.getEmsxNseFilled() == null ? null : or.getEmsxNseFilled());
    	ordemEMSX.setEmsxOrdRefId(or.getEmsxOrdRefId() == null ? null : or.getEmsxOrdRefId());
    	ordemEMSX.setEmsxOrderAsOfDate(or.getEmsxOrderAsOfDate() == null ? null : or.getEmsxOrderAsOfDate());
    	ordemEMSX.setEmsxOrderAsOfTimeMicrosec(or.getEmsxOrderAsOfTimeMicrosec() == null ? null : or.getEmsxOrderAsOfTimeMicrosec());
    	ordemEMSX.setEmsxOriginateTrader(or.getEmsxOriginateTrader() == null ? null : or.getEmsxOriginateTrader());
    	ordemEMSX.setEmsxOriginateTraderFirm(or.getEmsxOriginateTraderFirm() == null ? null : or.getEmsxOriginateTraderFirm());
    	ordemEMSX.setEmsxPercentRemain(or.getEmsxPercentRemain() == null ? null : or.getEmsxPercentRemain());
    	ordemEMSX.setEmsxPmUuid(or.getEmsxPmUuid() == null ? null : or.getEmsxPmUuid());
    	ordemEMSX.setEmsxPortMgr(or.getEmsxPortMgr() == null ? null : or.getEmsxPortMgr());
    	ordemEMSX.setEmsxPortName(or.getEmsxPortName() == null ? null : or.getEmsxPortName());
    	ordemEMSX.setEmsxPortNum(or.getEmsxPortNum() == null ? null : or.getEmsxPortNum());
    	ordemEMSX.setEmsxPosition(or.getEmsxPosition() == null ? null : or.getEmsxPosition());
    	ordemEMSX.setEmsxPrinciple(or.getEmsxPrinciple() == null ? null : or.getEmsxPrinciple());
    	ordemEMSX.setEmsxProduct(or.getEmsxProduct() == null ? null : or.getEmsxProduct());
    	ordemEMSX.setEmsxQueuedDate(or.getEmsxQueuedDate() == null ? null : or.getEmsxQueuedDate());
    	ordemEMSX.setEmsxQueuedTime(or.getEmsxQueuedTime() == null ? null : or.getEmsxQueuedTime());
    	ordemEMSX.setEmsxQueuedTimeMicrosec(or.getEmsxQueuedTimeMicrosec() == null ? null : or.getEmsxQueuedTimeMicrosec());
    	ordemEMSX.setEmsxReasonCode(or.getEmsxReasonCode() == null ? null : or.getEmsxReasonCode());
    	ordemEMSX.setEmsxReasonDesc(or.getEmsxReasonDesc() == null ? null : or.getEmsxReasonDesc());
    	ordemEMSX.setEmsxRemainBalance(or.getEmsxRemainBalance() == null ? null : or.getEmsxRemainBalance());
    	ordemEMSX.setEmsxRouteId(or.getEmsxRouteId() == null ? null : or.getEmsxRouteId());
    	ordemEMSX.setEmsxRoutePrice(or.getEmsxRoutePrice() == null ? null : or.getEmsxRoutePrice());
    	ordemEMSX.setEmsxSecName(or.getEmsxSecName() == null ? null : or.getEmsxSecName());
    	ordemEMSX.setEmsxSedol(or.getEmsxSedol() == null ? null : or.getEmsxSedol());
    	ordemEMSX.setEmsxSequence(or.getEmsxSequence() == null ? null : or.getEmsxSequence());
    	ordemEMSX.setEmsxSettleAmount(or.getEmsxSettleAmount() == null ? null : or.getEmsxSettleAmount());
    	ordemEMSX.setEmsxSettleDate(or.getEmsxSettleDate() == null ? null : or.getEmsxSettleDate());
    	ordemEMSX.setEmsxSi(or.getEmsxSi() == null ? null : or.getEmsxSi());
    	ordemEMSX.setEmsxStartAmount(or.getEmsxStartAmount() == null ? null : or.getEmsxStartAmount());
    	ordemEMSX.setEmsxStatus(or.getEmsxStatus() == null ? null : or.getEmsxStatus());
    	ordemEMSX.setEmsxStepOutBroker(or.getEmsxStepOutBroker() == null ? null : or.getEmsxStepOutBroker());
    	ordemEMSX.setEmsxStopPrice(or.getEmsxStopPrice() == null ? null : or.getEmsxStopPrice());
    	ordemEMSX.setEmsxStrategyEndTime(or.getEmsxStrategyEndTime() == null ? null : or.getEmsxStrategyEndTime());
    	ordemEMSX.setEmsxStrategyPartRate1(or.getEmsxStrategyPartRate1() == null ? null : or.getEmsxStrategyPartRate1());
    	ordemEMSX.setEmsxStrategyPartRate2(or.getEmsxStrategyPartRate2() == null ? null : or.getEmsxStrategyPartRate2());
    	ordemEMSX.setEmsxStrategyStartTime(or.getEmsxStrategyStartTime() == null ? null : or.getEmsxStrategyStartTime());
    	ordemEMSX.setEmsxStrategyStyle(or.getEmsxStrategyStyle() == null ? null : or.getEmsxStrategyStyle());
    	ordemEMSX.setEmsxStrategyType(or.getEmsxStrategyType() == null ? null : or.getEmsxStrategyType());
    	ordemEMSX.setEmsxTimeStamp(or.getEmsxTimeStamp() == null ? null : or.getEmsxTimeStamp());
    	ordemEMSX.setEmsxTimeStampMicrosec(or.getEmsxTimeStampMicrosec() == null ? null : or.getEmsxTimeStampMicrosec());
    	ordemEMSX.setEmsxTradUuid(or.getEmsxTradUuid() == null ? null : or.getEmsxTradUuid());
    	ordemEMSX.setEmsxTradeDesk(or.getEmsxTradeDesk() == null ? null : or.getEmsxTradeDesk());
    	ordemEMSX.setEmsxTraderNotes(or.getEmsxTraderNotes() == null ? null : or.getEmsxTraderNotes());
    	ordemEMSX.setEmsxTsOrdnum(or.getEmsxTsOrdnum() == null ? null : or.getEmsxTsOrdnum());
    	ordemEMSX.setEmsxType(or.getEmsxType() == null ? null : or.getEmsxType());
    	ordemEMSX.setEmsxUnderlyingTicker(or.getEmsxUnderlyingTicker() == null ? null : or.getEmsxUnderlyingTicker());
    	ordemEMSX.setEmsxUserCommAmount(or.getEmsxUserCommAmount() == null ? null : or.getEmsxUserCommAmount());
    	ordemEMSX.setEmsxCommRate(or.getEmsxCommRate() == null ? null : or.getEmsxCommRate());
    	ordemEMSX.setEmsxUserFees(or.getEmsxUserFees() == null ? null : or.getEmsxUserFees());
    	ordemEMSX.setEmsxUserNetMoney(or.getEmsxUserNetMoney() == null ? null : or.getEmsxUserNetMoney());
    	ordemEMSX.setEmsxWorkPrice(or.getEmsxWorkPrice() == null ? null : or.getEmsxWorkPrice());
    	ordemEMSX.setEmsxWorking(or.getEmsxWorking() == null ? null : or.getEmsxWorking());
    	ordemEMSX.setEmsxYellowKey(or.getEmsxYellowKey() == null ? null : or.getEmsxYellowKey());
		return ordemEMSX;
	}

	public BloombergOrder()
    {
    	// Define the service required, in this case the EMSX beta service, 
    	// and the values to be used by the SessionOptions object
    	// to identify IP/port of the back-end process.
    	d_service = "//blp/emapisvc_beta";
    	d_host = "localhost";
        d_port = 8194;
    }

    private void run(OrdemEmsxRequest ordem) throws Exception
    {
    	SessionOptions d_sessionOptions = new SessionOptions();
        d_sessionOptions.setServerHost(d_host);
        d_sessionOptions.setServerPort(d_port);
        Session session = new Session(d_sessionOptions);
        session.start();
        processSessionEvent(session, ordem);
    }
    
	private void processSessionEvent(Session session, OrdemEmsxRequest ordem) throws Exception {
    	session.openService(d_service);
    	com.bloomberglp.blpapi.Service service = session.getService(d_service);

    	Request request = service.createRequest(ordem.getAction().toString());
    	if(ordem.getAction().toString().equals("DeleteOrder")) {
    		request.getElement("EMSX_SEQUENCE").appendValue(ordem.getEmsxSequence());
    	}else {
    		if(ordem.getAction().toString().equals("ModifyOrder")) {
        		request.set("EMSX_SEQUENCE", ordem.getEmsxSequence());
    		}else {    			
    			request.set("EMSX_ORDER_REF_ID", ordem.getEmsxOrdRefId());
    			
        	    //BUY
        	    request.set("EMSX_SIDE", ordem.getEmsxSide());
    		}
    		// CIEL3 BZ Equity
    	    request.set("EMSX_TICKER", ordem.getEmsxTicker());
    	    //1000
    	    request.set("EMSX_AMOUNT", ordem.getEmsxAmount());
    	    //MKT
    	    request.set("EMSX_ORDER_TYPE", ordem.getEmsxOrderType());
    	    //DAY
    	    request.set("EMSX_TIF", ordem.getEmsxTif());
    	    //ANY
    	    request.set("EMSX_HAND_INSTRUCTION", ordem.getEmsxHandInstruction());

    	
    	    // The fields below are optional
    	    //request.set("EMSX_ACCOUNT","TestAccount");
    	    //request.set("EMSX_BASKET_NAME", "HedgingBasket");
    	    //request.set("EMSX_BROKER", "BMTB");
    	    //request.set("EMSX_CFD_FLAG", "1");
    	    //request.set("EMSX_CLEARING_ACCOUNT", "ClrAccName");
    	    //request.set("EMSX_CLEARING_FIRM", "FirmName");
    	    //request.set("EMSX_CUSTOM_NOTE1", "Note1");
    	    //request.set("EMSX_CUSTOM_NOTE2", "Note2");
    	    //request.set("EMSX_CUSTOM_NOTE3", "Note3");
    	    //request.set("EMSX_CUSTOM_NOTE4", "Note4");
    	    //request.set("EMSX_CUSTOM_NOTE5", "Note5");
    	    //request.set("EMSX_EXCHANGE_DESTINATION", "ExchDest");
    	    //request.set("EMSX_EXEC_INSTRUCTION", "Drop down values from EMSX Ticket");
    	    //request.set("EMSX_GET_WARNINGS", "0");
    	    //request.set("EMSX_GTD_DATE", "20170105");
    	    //request.set("EMSX_INVESTOR_ID", "InvID");
    	    //request.set("EMSX_LIMIT_PRICE", 123.45);
    	    //request.set("EMSX_LOCATE_BROKER", "BMTB");
    	    //request.set("EMSX_LOCATE_ID", "SomeID");
    	    //request.set("EMSX_LOCATE_REQ", "Y");
    	    //request.set("EMSX_NOTES", "Some notes");
    	    //request.set("EMSX_ODD_LOT", "0");
    	    //request.set("EMSX_ORDER_ORIGIN", "");
    	    //request.set("EMSX_ORDER_REF_ID", "UniqueID");
    	    //request.set("EMSX_P_A", "P");
    	    //request.set("EMSX_RELEASE_TIME", 34341);
    	    //request.set("EMSX_REQUEST_SEQ", 1001);
    	    //request.set("EMSX_SETTLE_CURRENCY", "USD");
    	    //request.set("EMSX_SETTLE_DATE", 20170106);
    	    //request.set("EMSX_SETTLE_TYPE", "T+2");
    	    //request.set("EMSX_STOP_PRICE", 123.5);

    	    System.out.println("Request: " + request.toString());
    	}

        requestID = new CorrelationID("");
        
        // Submit the request
    	try {
            session.sendRequest(request, requestID);
    	} catch (Exception ex) {
    		System.err.println("Failed to send the request");
    	}
      	session.stop();
	}	

}
