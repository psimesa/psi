package br.com.psi.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.entity.OrdemEmsx;
import br.com.psi.repository.OrdemEmsxRepository;

@Service
public class BloombergOrderTerminal {
	
	@Autowired
	private OrdemEmsxRepository repository;

	@RabbitListener(queues = {"${queue.bloomberg.receive-order}"})
	public void terminal(@Payload String msg) {
    	Gson gson = new Gson();
    	OrdemEmsxRequest order = gson.fromJson(msg, OrdemEmsxRequest.class);
    	if(order.getEmsxOrdRefId() != null) {
    		this.updateOrder(order);
    	}
	}

	private void updateOrder(OrdemEmsxRequest order) {
		OrdemEmsx orr = this.repository.findByEmsxOrdRefId(order.getEmsxOrdRefId());
		this.repository.save(build(orr, order));
	}
	
	private OrdemEmsx build(OrdemEmsx order, OrdemEmsxRequest or) {
		OrdemEmsx ordemEMSX = new OrdemEmsx();
		
		ordemEMSX.setId(order.getId());
		//Campos Obrigatórios para enviar a ordem para o Terminal Bloomberg
    	ordemEMSX.setEmsxAmount(or.getEmsxAmount());
    	ordemEMSX.setEmsxHandInstruction(or.getEmsxHandInstruction());
    	ordemEMSX.setEmsxOrderType(or.getEmsxOrderType());
    	ordemEMSX.setEmsxTicker(or.getEmsxTicker());
    	ordemEMSX.setEmsxTif(or.getEmsxTif());
    	ordemEMSX.setEmsxOrdRefId(or.getEmsxOrdRefId());
    	ordemEMSX.setEmsxSide(or.getEmsxSide());
    	
    	//Campos complementares
    	ordemEMSX.setApiSeqNum(or.getApiSeqNum() == null ? null : or.getApiSeqNum());
    	ordemEMSX.setEmsxAccount(or.getEmsxAccount() == null ? null : or.getEmsxAccount());
    	ordemEMSX.setEmsxArrivalPrice(or.getEmsxArrivalPrice() == null ? null : or.getEmsxArrivalPrice());
    	ordemEMSX.setEmsxAssetClass(or.getEmsxAssetClass() == null ? null : or.getEmsxAssetClass());
    	ordemEMSX.setEmsxAssignedTrader(or.getEmsxAssignedTrader() == null ? null : or.getEmsxAssignedTrader());
    	ordemEMSX.setEmsxAvgPrice(or.getEmsxAvgPrice() == null ? null : or.getEmsxAvgPrice());
    	ordemEMSX.setEmsxBasketName(or.getEmsxBasketName() == null ? null : or.getEmsxBasketName());
    	ordemEMSX.setEmsxBlockId(or.getEmsxBlockId() == null ? null : or.getEmsxBlockId());
    	ordemEMSX.setEmsxBroker(or.getEmsxBroker() == null ? null : or.getEmsxBroker());
    	ordemEMSX.setEmsxBrokerComm(or.getEmsxBrokerComm() == null ? null : or.getEmsxBrokerComm());
    	ordemEMSX.setEmsxBseAvgPrice(or.getEmsxBseAvgPrice() == null ? null : or.getEmsxBseAvgPrice());
    	ordemEMSX.setEmsxBseFilled(or.getEmsxBseFilled() == null ? null : or.getEmsxBseFilled());
    	ordemEMSX.setEmsxBuysideLei(or.getEmsxBuysideLei() == null ? null : or.getEmsxBuysideLei());
    	ordemEMSX.setEmsxClientIdentification(or.getEmsxClientIdentification() == null ? null : or.getEmsxClientIdentification());
    	ordemEMSX.setEmsxCfdFlag(or.getEmsxCfdFlag() == null ? null : or.getEmsxCfdFlag());
    	ordemEMSX.setEmsxCommDiffFlag(or.getEmsxCommDiffFlag() == null ? null : or.getEmsxCommDiffFlag());
    	ordemEMSX.setEmsxCommRate(or.getEmsxCommRate() == null ? null : or.getEmsxCommRate());
    	ordemEMSX.setEmsxCurrencyPair(or.getEmsxCurrencyPair() == null ? null : or.getEmsxCurrencyPair());
    	ordemEMSX.setEmsxDate(or.getEmsxDate() == null ? null : or.getEmsxDate());
    	ordemEMSX.setEmsxDayAvgPrice(or.getEmsxDayAvgPrice() == null ? null : or.getEmsxDayAvgPrice());
    	ordemEMSX.setEmsxDayFill(or.getEmsxDayFill() == null ? null : or.getEmsxDayFill());
    	ordemEMSX.setEmsxDirBrokerFlag(or.getEmsxDirBrokerFlag() == null ? null : or.getEmsxDirBrokerFlag());
    	ordemEMSX.setEmsxExchange(or.getEmsxExchange() == null ? null : or.getEmsxExchange());
    	ordemEMSX.setEmsxExchangeDestination(or.getEmsxExchangeDestination() == null ? null : or.getEmsxExchangeDestination());
    	ordemEMSX.setEmsxExecInstruction(or.getEmsxExecInstruction() == null ? null : or.getEmsxExecInstruction());
    	ordemEMSX.setEmsxFillId(or.getEmsxFillId() == null ? null : or.getEmsxFillId());
    	ordemEMSX.setEmsxFilled(or.getEmsxFilled() == null ? null : or.getEmsxFilled());
    	ordemEMSX.setEmsxGpi(or.getEmsxGpi() == null ? null : or.getEmsxGpi());
    	ordemEMSX.setEmsxGtdDate(or.getEmsxGtdDate() == null ? null : or.getEmsxGtdDate());
    	ordemEMSX.setEmsxIdleAmount(or.getEmsxIdleAmount() == null ? null : or.getEmsxIdleAmount());
    	ordemEMSX.setEmsxInvestorId(or.getEmsxInvestorId() == null ? null : or.getEmsxInvestorId());
    	ordemEMSX.setEmsxIsin(or.getEmsxIsin() == null ? null : or.getEmsxIsin());
    	ordemEMSX.setEmsxLimitPrice(or.getEmsxLimitPrice() == null ? null : or.getEmsxLimitPrice());
    	ordemEMSX.setEmsxMifidIiInstruction(or.getEmsxMifidIiInstruction() == null ? null : or.getEmsxMifidIiInstruction());
    	ordemEMSX.setEmsxModPendStatus(or.getEmsxModPendStatus() == null ? null : or.getEmsxModPendStatus());
    	ordemEMSX.setEmsxNotes(or.getEmsxNotes() == null ? null : or.getEmsxNotes());
    	ordemEMSX.setEmsxNseAvgPrice(or.getEmsxNseAvgPrice() == null ? null : or.getEmsxNseAvgPrice());
    	ordemEMSX.setEmsxNseFilled(or.getEmsxNseFilled() == null ? null : or.getEmsxNseFilled());
    	ordemEMSX.setEmsxOrderAsOfDate(or.getEmsxOrderAsOfDate() == null ? null : or.getEmsxOrderAsOfDate());
    	ordemEMSX.setEmsxOrderAsOfTimeMicrosec(or.getEmsxOrderAsOfTimeMicrosec() == null ? null : or.getEmsxOrderAsOfTimeMicrosec());
    	ordemEMSX.setEmsxOriginateTrader(or.getEmsxOriginateTrader() == null ? null : or.getEmsxOriginateTrader());
    	ordemEMSX.setEmsxOriginateTraderFirm(or.getEmsxOriginateTraderFirm() == null ? null : or.getEmsxOriginateTraderFirm());
    	ordemEMSX.setEmsxPercentRemain(or.getEmsxPercentRemain() == null ? null : or.getEmsxPercentRemain());
    	ordemEMSX.setEmsxPmUuid(or.getEmsxPmUuid() == null ? null : or.getEmsxPmUuid());
    	ordemEMSX.setEmsxPortMgr(or.getEmsxPortMgr() == null ? null : or.getEmsxPortMgr());
    	ordemEMSX.setEmsxPortName(or.getEmsxPortName() == null ? null : or.getEmsxPortName());
    	ordemEMSX.setEmsxPortNum(or.getEmsxPortNum() == null ? null : or.getEmsxPortNum());
    	ordemEMSX.setEmsxPosition(or.getEmsxPosition() == null ? null : or.getEmsxPosition());
    	ordemEMSX.setEmsxPrinciple(or.getEmsxPrinciple() == null ? null : or.getEmsxPrinciple());
    	ordemEMSX.setEmsxProduct(or.getEmsxProduct() == null ? null : or.getEmsxProduct());
    	ordemEMSX.setEmsxQueuedDate(or.getEmsxQueuedDate() == null ? null : or.getEmsxQueuedDate());
    	ordemEMSX.setEmsxQueuedTime(or.getEmsxQueuedTime() == null ? null : or.getEmsxQueuedTime());
    	ordemEMSX.setEmsxQueuedTimeMicrosec(or.getEmsxQueuedTimeMicrosec() == null ? null : or.getEmsxQueuedTimeMicrosec());
    	ordemEMSX.setEmsxReasonCode(or.getEmsxReasonCode() == null ? null : or.getEmsxReasonCode());
    	ordemEMSX.setEmsxReasonDesc(or.getEmsxReasonDesc() == null ? null : or.getEmsxReasonDesc());
    	ordemEMSX.setEmsxRemainBalance(or.getEmsxRemainBalance() == null ? null : or.getEmsxRemainBalance());
    	ordemEMSX.setEmsxRouteId(or.getEmsxRouteId() == null ? null : or.getEmsxRouteId());
    	ordemEMSX.setEmsxRoutePrice(or.getEmsxRoutePrice() == null ? null : or.getEmsxRoutePrice());
    	ordemEMSX.setEmsxSecName(or.getEmsxSecName() == null ? null : or.getEmsxSecName());
    	ordemEMSX.setEmsxSedol(or.getEmsxSedol() == null ? null : or.getEmsxSedol());
    	ordemEMSX.setEmsxSequence(or.getEmsxSequence() == null ? null : or.getEmsxSequence());
    	ordemEMSX.setEmsxSettleAmount(or.getEmsxSettleAmount() == null ? null : or.getEmsxSettleAmount());
    	ordemEMSX.setEmsxSettleDate(or.getEmsxSettleDate() == null ? null : or.getEmsxSettleDate());
    	ordemEMSX.setEmsxSi(or.getEmsxSi() == null ? null : or.getEmsxSi());
    	ordemEMSX.setEmsxStartAmount(or.getEmsxStartAmount() == null ? null : or.getEmsxStartAmount());
    	ordemEMSX.setEmsxStatus(or.getEmsxStatus() == null ? null : or.getEmsxStatus());
    	ordemEMSX.setEmsxStepOutBroker(or.getEmsxStepOutBroker() == null ? null : or.getEmsxStepOutBroker());
    	ordemEMSX.setEmsxStopPrice(or.getEmsxStopPrice() == null ? null : or.getEmsxStopPrice());
    	ordemEMSX.setEmsxStrategyEndTime(or.getEmsxStrategyEndTime() == null ? null : or.getEmsxStrategyEndTime());
    	ordemEMSX.setEmsxStrategyPartRate1(or.getEmsxStrategyPartRate1() == null ? null : or.getEmsxStrategyPartRate1());
    	ordemEMSX.setEmsxStrategyPartRate2(or.getEmsxStrategyPartRate2() == null ? null : or.getEmsxStrategyPartRate2());
    	ordemEMSX.setEmsxStrategyStartTime(or.getEmsxStrategyStartTime() == null ? null : or.getEmsxStrategyStartTime());
    	ordemEMSX.setEmsxStrategyStyle(or.getEmsxStrategyStyle() == null ? null : or.getEmsxStrategyStyle());
    	ordemEMSX.setEmsxStrategyType(or.getEmsxStrategyType() == null ? null : or.getEmsxStrategyType());
    	ordemEMSX.setEmsxTimeStamp(or.getEmsxTimeStamp() == null ? null : or.getEmsxTimeStamp());
    	ordemEMSX.setEmsxTimeStampMicrosec(or.getEmsxTimeStampMicrosec() == null ? null : or.getEmsxTimeStampMicrosec());
    	ordemEMSX.setEmsxTradUuid(or.getEmsxTradUuid() == null ? null : or.getEmsxTradUuid());
    	ordemEMSX.setEmsxTradeDesk(or.getEmsxTradeDesk() == null ? null : or.getEmsxTradeDesk());
    	ordemEMSX.setEmsxTraderNotes(or.getEmsxTraderNotes() == null ? null : or.getEmsxTraderNotes());
    	ordemEMSX.setEmsxTsOrdnum(or.getEmsxTsOrdnum() == null ? null : or.getEmsxTsOrdnum());
    	ordemEMSX.setEmsxType(or.getEmsxType() == null ? null : or.getEmsxType());
    	ordemEMSX.setEmsxUnderlyingTicker(or.getEmsxUnderlyingTicker() == null ? null : or.getEmsxUnderlyingTicker());
    	ordemEMSX.setEmsxUserCommAmount(or.getEmsxUserCommAmount() == null ? null : or.getEmsxUserCommAmount());
    	ordemEMSX.setEmsxCommRate(or.getEmsxCommRate() == null ? null : or.getEmsxCommRate());
    	ordemEMSX.setEmsxUserFees(or.getEmsxUserFees() == null ? null : or.getEmsxUserFees());
    	ordemEMSX.setEmsxUserNetMoney(or.getEmsxUserNetMoney() == null ? null : or.getEmsxUserNetMoney());
    	ordemEMSX.setEmsxWorkPrice(or.getEmsxWorkPrice() == null ? null : or.getEmsxWorkPrice());
    	ordemEMSX.setEmsxWorking(or.getEmsxWorking() == null ? null : or.getEmsxWorking());
    	ordemEMSX.setEmsxYellowKey(or.getEmsxYellowKey() == null ? null : or.getEmsxYellowKey());
    	return ordemEMSX;
	}
	
}
