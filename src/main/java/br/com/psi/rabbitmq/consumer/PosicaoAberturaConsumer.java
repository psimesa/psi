package br.com.psi.rabbitmq.consumer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import br.com.psi.dto.response.CotaFundoInvestimentoResponse;
import br.com.psi.dto.response.FundoResponse;
import br.com.psi.dto.response.TickerResponse;
import br.com.psi.entity.PosicaoAbertura;
import br.com.psi.repository.PosicaoAberturaRepository;
import br.com.psi.service.CotaFundoInvestimentoService;
import br.com.psi.service.FundoService;
import br.com.psi.service.TickerService;

@Service
public class PosicaoAberturaConsumer {
	
	@Autowired
	private PosicaoAberturaRepository repository;
	
	@Autowired
	private FundoService fundoService;
	
	@Autowired
	private TickerService tickerService;
	
	@Autowired
	private CotaFundoInvestimentoService cotaService;

	/* Método responsável por gravar posição de abertura do Fundo, enviado pelo Lote45.
	 * Payload é o fundo.
	*/
	@RabbitListener(queues = {"${queue.lote45.opening-position}"})
	public void receive(@Payload ArrayList<String> fundo) {
		PosicaoAbertura pd = new PosicaoAbertura();
		FundoResponse fres = fundoService.getByFundo(fundo.get(0));
		TickerResponse tres = tickerService.getByTicker(fundo.get(1));
		CotaFundoInvestimentoResponse cres = cotaService.getByName(fundo.get(2));
		
		pd.setFundo(fres.getId());
		pd.setProduct(tres.getId());
		pd.setBook(cres.getId());
		pd.setFinancialPu(new BigDecimal(fundo.get(3)).setScale(2, RoundingMode.HALF_EVEN));
		pd.setAmount(Integer.parseInt(fundo.get(4)));
		repository.save(pd);
	}
	
}
