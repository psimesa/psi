package br.com.psi.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.psi.dto.request.OrdemEmsxRequest;
import br.com.psi.entity.OrdemEmsx;
import br.com.psi.repository.OrdemEmsxRepository;
import br.com.psi.util.MethodsUtils;

@Service
public class BloombergRouteTerminal {
	
	@Autowired
	private OrdemEmsxRepository repository;
	
	@Autowired
	private MethodsUtils utils;

//	@RabbitListener(queues = {"${queue.bloomberg.terminal}"})
	public void terminal(@Payload String msg) {
    	Gson gson = new Gson();
    	OrdemEmsxRequest order = gson.fromJson(msg, OrdemEmsxRequest.class);
    	if(order.getEmsxOrdRefId() != null) {
    		this.updateRoute(order);
    	}else {
    		this.insertNewRoute(order);
    	}
	}

	private void updateRoute(OrdemEmsxRequest order) {
		OrdemEmsx orr = this.repository.findByEmsxOrdRefId(order.getEmsxOrdRefId());
		order.setEmsxOrdRefId(orr.getEmsxOrdRefId());
		this.repository.save((OrdemEmsx) this.utils.convertToEntity(order, orr));
	}

	private void insertNewRoute(OrdemEmsxRequest order) {
		//TODO Verificar como será implementado quando tiver uma ordem que for criada direto na Bloomberg

	}
	
}
