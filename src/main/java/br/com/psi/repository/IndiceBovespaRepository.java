package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.IndiceBovespa;

@Repository
public interface IndiceBovespaRepository extends JpaRepository<IndiceBovespa, Integer>{

	IndiceBovespa findByTicker(String name);
	
}

