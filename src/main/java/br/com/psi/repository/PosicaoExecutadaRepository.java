package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.PosicaoExecutada;

@Repository
public interface PosicaoExecutadaRepository extends JpaRepository<PosicaoExecutada, Integer>{

	PosicaoExecutada findByProductAndBookAndFundo(String product, String book, String fund);
	
	long deleteByIdOrdemDetail(Integer idOrdemDetail);
	
}

