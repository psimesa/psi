package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.RegraEnquadramento;

@Repository
public interface RegraEnquadramentoRepository extends JpaRepository<RegraEnquadramento, Integer>{

}