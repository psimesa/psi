package br.com.psi.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.Ordem;
import br.com.psi.enums.Status;

@Repository
public interface OrdemRepository extends JpaRepository<Ordem, Integer>{

	List<Ordem> findByStatusInAndDataInclusaoFilter(List<Status> status, LocalDate data, Sort sort);
	
}

