package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.GrupoEnquadramento;

@Repository
public interface GrupoEnquadramentoRepository extends JpaRepository<GrupoEnquadramento, Integer>{

}