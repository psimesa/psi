package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.TipoOperacaoOrdem;

@Repository
public interface TipoOperacaoOrdemRepository extends JpaRepository<TipoOperacaoOrdem, Integer>{

}