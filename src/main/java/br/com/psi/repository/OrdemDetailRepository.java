package br.com.psi.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.OrdemDetail;
import br.com.psi.enums.Status;

@Repository
public interface OrdemDetailRepository extends JpaRepository<OrdemDetail, Integer>{

	public List<OrdemDetail> findByStatusInAndDate(List<Status> status, LocalDate data);
	
	public List<OrdemDetail> findByIdOrdem(Integer idOrdem);
	
}

