package br.com.psi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.PosicaoPrevista;

@Repository
public interface PosicaoPrevistaRepository extends JpaRepository<PosicaoPrevista, Integer>{

	public List<PosicaoPrevista> findByProductAndBookAndFundo(Integer product, Integer book, Integer fund);

	public long deleteByIdOrdemDetail(Integer idOrdemDetail);
	
}

