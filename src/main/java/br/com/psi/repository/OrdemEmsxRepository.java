package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.OrdemEmsx;

@Repository
public interface OrdemEmsxRepository extends JpaRepository<OrdemEmsx, Integer>{

	OrdemEmsx findByEmsxOrdRefId(Integer emsxOrdRefId);
	
	long deleteByEmsxOrdRefId(Integer emsxOrdRefId);

}

