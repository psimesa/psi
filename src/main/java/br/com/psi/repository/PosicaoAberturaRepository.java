package br.com.psi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.PosicaoAbertura;

@Repository
public interface PosicaoAberturaRepository extends JpaRepository<PosicaoAbertura, Integer>{

	PosicaoAbertura findByProductAndBookAndFundo(Integer product, Integer book, Integer fundo);
	
	List<PosicaoAbertura> findByFundoAndBook(Integer fundo, Integer book);
	
}

