package br.com.psi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.CotaFundoInvestimento;

@Repository
public interface CotaFundoInvestimentoRepository extends JpaRepository<CotaFundoInvestimento, Integer>{
	
	public List<CotaFundoInvestimento> findByFundo(Integer idFundo);
	
	public CotaFundoInvestimento findByNome(String nome);

}

