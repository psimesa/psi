package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.Rebalance;

@Repository
public interface RebalanceRepository extends JpaRepository<Rebalance, Integer>{

}

