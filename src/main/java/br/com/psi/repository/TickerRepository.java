package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.Ticker;

@Repository
public interface TickerRepository extends JpaRepository<Ticker, Integer>{

	Ticker findByName(String name);
	
}

