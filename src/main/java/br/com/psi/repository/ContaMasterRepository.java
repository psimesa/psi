package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.ContaMaster;

@Repository
public interface ContaMasterRepository extends JpaRepository<ContaMaster, Integer>{

}

