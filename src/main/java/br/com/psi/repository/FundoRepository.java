package br.com.psi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.psi.entity.Fundo;

@Repository
public interface FundoRepository extends JpaRepository<Fundo, Integer>{

	Fundo findByFundo(String fundo);
	
	
}

