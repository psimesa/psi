package br.com.psi.configurations;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
	
	@Value("${queue.bloomberg.send-order}")
	private String sendOrderBloomberg;
	
	@Value("${queue.bloomberg.receive-order}")
	private String receiveOrderBloomberg;
	
	@Value("${queue.lote45.opening-position}")
	private String openingPosition;
	
	@Value("${exchange.bloomberg}")
	private String bloombergExchange;
	
	@Value("${exchange.lote45}")
	private String lote45Exchange;

	@Bean
	DirectExchange bloombergExchange() {
		return new DirectExchange(bloombergExchange);
	}
	
	@Bean
	DirectExchange exchangeLote45() {
		return new DirectExchange(lote45Exchange);
	}
	
	@Bean
	Queue senOrderTerminalBloomberg() {
		return new Queue(sendOrderBloomberg, false);
	}
	
	@Bean
	Queue receiveOrderBloomberg() {
		return new Queue(receiveOrderBloomberg, false);
	}
	
	@Bean
	Queue openingPosition() {
		return new Queue(openingPosition, false);
	}
	
	@Bean
	Binding receiveOrderBloombergBinding(@Qualifier("receiveOrderBloomberg") Queue receiveOrderBloombergQueue, @Qualifier("bloombergExchange") DirectExchange exchange) {
		return BindingBuilder.bind(receiveOrderBloombergQueue).to(exchange).with("receiveOrderBloomberg");
	}
	
	@Bean
	Binding sendOrderBloombergBinding(@Qualifier("senOrderTerminalBloomberg") Queue sendOrderBloombergQueue, @Qualifier("bloombergExchange") DirectExchange exchange) {
		return BindingBuilder.bind(sendOrderBloombergQueue).to(exchange).with("sendOrderBloomberg");
	}
	
	@Bean
	Binding getOpeningPosition(@Qualifier("openingPosition") Queue openingPosition, @Qualifier("exchangeLote45") DirectExchange exchange) {
		return BindingBuilder.bind(openingPosition).to(exchange).with("openingPosition");
	}

}
