package br.com.psi.configurations;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A classe CORSFilter.
 */
@Component
public class CORSFilter implements Filter {
    
    /**
     * Inicialização.
     *
     * @param filterConfig o filtro de configuração
     * @throws ServletException a ServletException
     */
    @Override
    public  void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Filtra.
     *
     * @param request a requisição
     * @param response a resposta
     * @param chain a sequência de filtro
     * @throws IOException Sinaliza que uma exceção de I/O ocorreu.
     * @throws ServletException a ServletException
     */
    @Override
    public  void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (response instanceof HttpServletResponse) {
            HttpServletResponse r = (HttpServletResponse) response;
            r.setHeader("Access-Control-Allow-Origin", "*");
            r.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PATCH, PUT");
            r.setHeader("Access-Control-Max-Age", "3600");
            r.setHeader("Access-Control-Allow-Headers", "*");
            chain.doFilter(request, response);
        } else {
            throw new ServletException("CORS works only for HTTP");
        }
    }

    /**
     * Destroy.
     */
    @Override
    public  void destroy() {
    }
}
