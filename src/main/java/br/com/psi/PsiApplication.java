package br.com.psi;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan({ "br.com.psi.*" })
@EntityScan("br.com.psi.*")
@SpringBootApplication
@EnableScheduling
public class PsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PsiApplication.class, args);
	}
	
	/**
	 * Model mapper.
	 *
	 * @return o  mapeador do modelo.
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new org.modelmapper.ModelMapper();
	}

}
