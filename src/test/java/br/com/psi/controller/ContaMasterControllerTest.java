package br.com.psi.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.psi.dto.response.ContaMasterResponse;
import br.com.psi.service.ContaMasterService;

/**
 * A classe ContaMasterControllerTest, que testa o ContaMasterController.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ContaMasterController.class)
public class ContaMasterControllerTest {
	
	  /** o id da conta master. */
	  private static final Integer ID   = 1;
	  
	  /** o nome da conta master. */
	  private static final String NAME = "PORTO";
	  
	  /** o mvc. */
	  @Autowired
	  private MockMvc mvc;
	 
	  @MockBean
	  private ContaMasterService service;
	  
	  @Test
	  public void testGetAll() throws Exception {
	        List<ContaMasterResponse> list = new ArrayList<>();
	        ContaMasterResponse res = new ContaMasterResponse();
	        res.setNome(NAME);
	        list.add(res);
	        when(this.service.getAll()).thenReturn(list);
	        

	        mvc.perform(MockMvcRequestBuilders
	                .get("/conta-master/get-all")
	                .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isOk())
	                .andExpect(status().isOk());
	  }

}
